package com.chess.gui;

import com.chess.game.Board.*;
import com.chess.game.Piece.Piece;
import com.chess.game.Piece.PieceType;
import com.google.common.collect.Lists;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

/*************************************************************************
 * Window Class represents the Chess Game main GUI
 * "WINDOW_DIMENSION": size of the window..
 * "BOARD_PANEL_DIMENSION": size of the chess board.
 * "TILE_PANEL_DIMENSION": size of the tile.
 * "m_WindowFrame": The frame of GUI
 * "m_BoardPanel": The panel shows chessboard.
 * "m_TakenPiecesPanel": The panel show pieces that has been taken by players.
 * "m_GameHistoryPanel": The panel show moves have been made.
 * "m_ChessBoard": The current state of the chess board.
 * "m_moveHistory": The move log of the game.
 * "m_CurrentBoardDiretion": Is the board is flipped or not?
 * "m_SourceTile": The tile that player choose on first click.
 * "m_DestinationTile": The tile that player choose on second click.
 * "m_PieceChoosenByPlayer": The piece that player choose on first click.
 * "m_HighlightLegals": Player want to see possible move on choosen piece or not?
 */
public class Window
{
	private static final Dimension WINDOW_DIMENSION = new Dimension(768, 768);
	private static final Dimension BOARD_PANEL_DIMENSION = new Dimension(500, 450);
	private static final Dimension TILE_PANEL_DIMENSION = new Dimension(10, 10);
	private static final Color LIGHT_TILE_COLOR = Color.decode("#ffffff");
	private static final Color DARK_TILE_COLOR = Color.decode("#000000");
	private static final String PATH_TO_ICON_FOLDER = "import/";


	private final JFrame m_WindowFrame;
	private final BoardPanel m_BoardPanel;
	private final TakenPiecesPanel m_TakenPiecesPanel;
	private final GameHistoryPanel m_GameHistoryPanel;

	private Board m_ChessBoard;
	private MoveLog m_MoveHistory;
	private BoardDirection m_CurrentBoardDirection;

	private Tile m_SourceTile;
	private Tile m_DestinationTile;
	private Piece m_PieceChosenByPlayer;
	private boolean m_HighlightLegals;

	/**
	 * Window Constructor
	 * Create JFrame and add components to it.
	 * Add event listener resolving resize situation.
	 */
	public Window()
	{
		m_ChessBoard = Board.CreateStandardGameBoard();
		m_MoveHistory = new MoveLog();
		m_CurrentBoardDirection = BoardDirection.NORMAL;
		m_HighlightLegals = true;

		m_WindowFrame = new JFrame("Java Chess");
		m_WindowFrame.setLayout(new BorderLayout());

		final JMenuBar windowMenuBar = CreateMenuBar();
		m_WindowFrame.setJMenuBar(windowMenuBar);
		m_WindowFrame.setSize(WINDOW_DIMENSION);

		m_GameHistoryPanel = new GameHistoryPanel();
		m_TakenPiecesPanel = new TakenPiecesPanel(m_WindowFrame);

		m_BoardPanel = new BoardPanel(this);

		m_WindowFrame.add(m_TakenPiecesPanel, BorderLayout.WEST);
		m_WindowFrame.add(m_BoardPanel, BorderLayout.CENTER);
		m_WindowFrame.add(m_GameHistoryPanel, BorderLayout.EAST);


		m_WindowFrame.setVisible(true);

		m_WindowFrame.getRootPane().addComponentListener(new ComponentAdapter()
		{
			public void componentResized(ComponentEvent e)
			{
				// This is only called when the user releases the mouse button.
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				if(screenSize.height == 768)
				{
					WINDOW_DIMENSION.height = 768;
					WINDOW_DIMENSION.width = 768;

				}
				if(m_WindowFrame.getBounds().width < WINDOW_DIMENSION.width || m_WindowFrame.getBounds().height < WINDOW_DIMENSION.height)
				{

					JOptionPane.showMessageDialog(m_WindowFrame, "This window frame should not be resized smaller.");
					m_WindowFrame.setSize(WINDOW_DIMENSION);
				}
				m_BoardPanel.DrawBoard(m_ChessBoard);
				m_TakenPiecesPanel.ReDraw(m_MoveHistory);
				m_GameHistoryPanel.redo(m_ChessBoard, m_MoveHistory);
			}
		});

		m_WindowFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	}

	/**
	 * Create Menu Bar
	 * Returns the menu bar after add components to it.
	 */
	private JMenuBar CreateMenuBar()
	{
		final JMenuBar result = new JMenuBar();
		result.add(CreateFileMenu());
		result.add(CreatePreferenceMenu());
		return result;
	}

	/**
	 * Create File Menu
	 * Returns File Menu after add components to it.
	 */
	private JMenu CreateFileMenu()
	{
		final JMenu fileMenu = new JMenu("File");
		final JMenuItem about = new JMenuItem("About.");
		about.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				ImageIcon icon = new ImageIcon("import/emoticon.jpg");
				ImageIcon newI = new ImageIcon(icon.getImage().getScaledInstance(45,45, Image.SCALE_SMOOTH));
				JOptionPane.showMessageDialog(m_WindowFrame, "Made by:\n- Hoang Nhat Minh\n- Nguyen Phu Quang Anh\n- Ta Nguyen Ngoc\n- Dao Tuan Trung\nEnjoy playing!",
						"About us",JOptionPane.PLAIN_MESSAGE, newI);
				System.out.println("Welcome!");
			}
		});

		final JMenuItem SetupBoard = new JMenuItem("Setup Board Type");
		Object[] options = {"Standard Game Board", "King Rook Game Board", "Two Pawn King Game Board", "King Castle Game Board" };
		SetupBoard.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int choose = JOptionPane.showOptionDialog(m_WindowFrame, "Choose type of board you want to use.", "Board Type", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
				if(choose == 0)
				{
					RestartGame();
					m_ChessBoard = Board.CreateStandardGameBoard();
					m_BoardPanel.DrawBoard(m_ChessBoard);
				}
				else if(choose == 1)
				{
					RestartGame();
					m_ChessBoard = Board.CreateKingRookGameBoard();
					m_BoardPanel.DrawBoard(m_ChessBoard);
				}
				else if(choose == 2)
				{
					RestartGame();
					m_ChessBoard = Board.CreateTwoPawnGameBoard();
					m_BoardPanel.DrawBoard(m_ChessBoard);
				}
				else if(choose == 3)
				{
					RestartGame();
					m_ChessBoard = Board.CreateKingCaslteBug();
					m_BoardPanel.DrawBoard(m_ChessBoard);
				}
			}
		});
		fileMenu.add(SetupBoard);

		final JMenuItem undoMove = new JMenuItem("Undo Last Move");
		undoMove.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				UndoLastMove();
			}
		});
		fileMenu.add(undoMove);

		final JMenuItem restartGame = new JMenuItem("Restart Game");
		restartGame.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				RestartGame();
			}
		});
		fileMenu.add(restartGame);

		fileMenu.addSeparator();
		fileMenu.add(about);
		final JMenuItem exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		fileMenu.add(exit);

		return fileMenu;
	}

	/**
	 * Create Preference Menu
	 * Returns Preference Menu after add components to it.
	 */
	private JMenu CreatePreferenceMenu()
	{
		final JMenu preferenceMenu = new JMenu("Preference");
		final JMenuItem flipBoard = new JMenuItem("Flipboard");
		flipBoard.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				m_CurrentBoardDirection = m_CurrentBoardDirection.GetOpposite();
				m_BoardPanel.DrawBoard(m_ChessBoard);
			}
		});
		preferenceMenu.add(flipBoard);

		preferenceMenu.addSeparator();

		final JCheckBoxMenuItem highlightLegalCheckbox = new JCheckBoxMenuItem("Highlight legal m_MoveList.", true);
		highlightLegalCheckbox.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				m_HighlightLegals = highlightLegalCheckbox.isSelected();
			}
		});

		preferenceMenu.add(highlightLegalCheckbox);

		return preferenceMenu;
	}

	/**
	 * Restart Game
	 * Make chess board to be the intial state again.
	 */
	protected void RestartGame()
	{
		for(int i = m_MoveHistory.GetMoveList().size() - 1; i > -1; i--)
		{
			final Move lastMove = m_MoveHistory.RemoveMove(m_MoveHistory.GetMoveList().size() - 1);
			m_ChessBoard = m_ChessBoard.GetCurrentPlayer().MakeUndoMove(lastMove).GetResultBoard();
		}
		for(Piece piece : m_ChessBoard.GetAllActivePieces())
		{
			piece.SetIsFirstMove(true);
		}
		m_MoveHistory.Clear();
		m_GameHistoryPanel.redo(m_ChessBoard, m_MoveHistory);
		m_TakenPiecesPanel.ReDraw(m_MoveHistory);
		m_BoardPanel.DrawBoard(m_ChessBoard);
	}

	/**
	 * Undo Move
	 * Undo the previous move.
	 */
	private void UndoLastMove()
	{
		if(m_MoveHistory.GetMoveList().size() == 0)
		{
			JOptionPane.showMessageDialog(m_WindowFrame, "No move to undo!");
			return;
		}
		final Move lastMove = m_MoveHistory.RemoveMove(m_MoveHistory.GetMoveList().size() - 1);
		m_ChessBoard = m_ChessBoard.GetCurrentPlayer().MakeUndoMove(lastMove).GetResultBoard();
		m_MoveHistory.RemoveMove(lastMove);
		m_GameHistoryPanel.redo(m_ChessBoard, m_MoveHistory);
		m_TakenPiecesPanel.ReDraw(m_MoveHistory);
		m_BoardPanel.DrawBoard(m_ChessBoard);
	}

	//////////////////////////////// SUB - CLASS ///////////////////////////////////////
	/**
	 * BoardPanel class represent the chess board in GUI.
	 * "m_BoardTiles": consisting 64 Tile Panel, representing 64 tiles of the chess board.
	 * "m_Window": the window that consisting this board panel.
	 */
	private class BoardPanel extends JPanel
	{
		final List<TilePanel> m_BoardTiles;
		final Window m_Window;

		/**
		 * Board Panel's constructor
		 * Create a GridLayout (8 rows, 8 columns) and add TilePanel to those.
		 */
		BoardPanel(final Window window)
		{
			super(new GridLayout(8, 8));
			m_Window = window;
			m_BoardTiles = new ArrayList<>();
			for (int i = 0; i < BoardUtilities.NUM_TILES; i++)
			{
				final TilePanel tilePanel = new TilePanel(i);
				m_BoardTiles.add(tilePanel);
				add(tilePanel);
			}
			setPreferredSize(BOARD_PANEL_DIMENSION);
			validate();
		}

		/**
		 * Draw Board
		 * Draw the board to the UI.
		 */
		public void DrawBoard(final Board board)
		{
			removeAll();
			for(final TilePanel tilePanel : m_CurrentBoardDirection.Traverse(m_BoardTiles))
			{
				tilePanel.DrawTile(board);
				add(tilePanel);
			}

			// Re-layout its subcomponents
			validate();
			// Re-paint because we doesn't change its size, only its look.
			repaint();
			if(board.GetCurrentPlayer().IsInCheckMate() || board.GetCurrentPlayer().IsInStaleMate())
			{
				String alli = board.GetCurrentPlayer().GetAlliance().toString();
				String res = alli + " loses. Restart?";
				if (board.GetCurrentPlayer().IsInStaleMate())
				{
					res = "Tie. Restart?";
				}
				int choose = JOptionPane.showConfirmDialog(m_WindowFrame, res,"End game.", JOptionPane.YES_NO_OPTION);
				if(choose == 0)
				{
					m_Window.RestartGame();
				}
				else if (choose == 1)
				{
					System.exit(0);
				}
			}
		}
	}

	/**
	 * TilePanel class represent a tiel of chess board in GUI.
	 * "m_TileID": represents the ID/coordinate of that tile.
	 */
	private class TilePanel extends JPanel
	{
		private final int m_TileID;

		/**
		 * Tile Panel's constructor
		 * Assign Stuff to a tile and create a listener for mouse click.
		 */
		TilePanel(final int tileID)
		{
			super(new GridBagLayout());
			m_TileID = tileID;
			setPreferredSize(TILE_PANEL_DIMENSION);
			AssignTileColor();
			AssignTileIcon(m_ChessBoard);
			setBorder(BorderFactory.createLineBorder(Color.GRAY));
			addMouseListener(new MouseListener()
			{

				@Override
				public void mouseClicked(MouseEvent e)
				{
					if(isRightMouseButton(e))
					{
						ClearPlayerClickState();
					}
					else if(isLeftMouseButton(e))
					{
						System.out.println("Clicked" + " ");
						if(m_SourceTile == null)
						{
							m_SourceTile = m_ChessBoard.GetTile(m_TileID);
							if(m_SourceTile.IsTileOccupied())
							{
								m_PieceChosenByPlayer = m_SourceTile.GetPiece();
								if(m_PieceChosenByPlayer == null || m_PieceChosenByPlayer.GetAlliance() != m_ChessBoard.GetCurrentPlayer().GetAlliance())
								{
									m_SourceTile = null;
									m_PieceChosenByPlayer = null;
								}
							}
						}
						else
						{
							boolean runed = false;
							m_DestinationTile = m_ChessBoard.GetTile(m_TileID);
							if(m_DestinationTile.IsTileOccupied())
							{
								if(m_DestinationTile.GetPiece().GetAlliance() == m_ChessBoard.GetCurrentPlayer().GetAlliance())
								{
									m_SourceTile = m_DestinationTile;
									m_PieceChosenByPlayer = m_SourceTile.GetPiece();
									m_DestinationTile = null;
									runed = true;
								}
							}
							if(!runed)
							{
								final Move move = Move.MoveFactory.CreateMove(m_ChessBoard, m_SourceTile.GetTileCoordinate(), m_DestinationTile.GetTileCoordinate());
								boolean pieceIsFirstMove = true;
								if(!(move instanceof Move.NullMove))
								{
									pieceIsFirstMove = move.GetMovedPiece().GetIsFirstMove();
									move.GetMovedPiece().SetIsFirstMove(false);
								}
								final MovePreview movePreview = m_ChessBoard.GetCurrentPlayer().MakeMove(move);

								if(movePreview.GetMoveStatus() == MoveStatus.VALID_MOVE_COMPLETE)
								{
									m_ChessBoard = movePreview.GetResultBoard();
									m_MoveHistory.GetMoveList().add(move);
									if(m_ChessBoard.GetCurrentPlayer().IsInCheck())
									{
										JOptionPane.showMessageDialog(m_WindowFrame, "King is in check!");
									}
								}
								else
								{
									if(!(move instanceof Move.NullMove))
									{
										move.GetMovedPiece().SetIsFirstMove(pieceIsFirstMove);
									}
									//System.out.println("Invalid move.");
								}
								ClearPlayerClickState();
							}
						}
						SwingUtilities.invokeLater(new Runnable()
						{
							@Override
							public void run()
							{
								m_BoardPanel.DrawBoard(m_ChessBoard);
								m_TakenPiecesPanel.ReDraw(m_MoveHistory);
								m_GameHistoryPanel.redo(m_ChessBoard, m_MoveHistory);
							}
						});
					}
				}

				@Override
				public void mousePressed(MouseEvent e) {}

				@Override
				public void mouseReleased(MouseEvent e) {}

				@Override
				public void mouseEntered(MouseEvent e) {}

				@Override
				public void mouseExited(MouseEvent e) {}
			});
			validate();
		}

		/**
		 * Clear Player Click State
		 * Nullify properties involved in the clicking process.
		 */
		public void ClearPlayerClickState()
		{
			m_SourceTile = null;
			m_DestinationTile = null;
			m_PieceChosenByPlayer = null;
		}

		/**
		 * Assign Tile Icon
		 * Add the piece's icon to this tile.
		 */
		private void AssignTileIcon(final Board board)
		{
			this.removeAll();
			if(board.GetTile(m_TileID).IsTileOccupied())
			{
				final Piece piece = board.GetTile(m_TileID).GetPiece();
				String fileName = piece.GetAlliance().toString().substring(0, 1);
				fileName += piece.toString();
				String iconPath = PATH_TO_ICON_FOLDER + fileName + ".png";
				try
				{
					final BufferedImage image = ImageIO.read(new File(iconPath));
					final ImageIcon imageIcon = new ImageIcon(image);
					int targetImageSize = 80 * (m_WindowFrame.getBounds().width / WINDOW_DIMENSION.height) > 120 ? 120 : 80 * (m_WindowFrame.getBounds().width / WINDOW_DIMENSION.height);
					this.add(new JLabel(new ImageIcon(imageIcon.getImage().getScaledInstance(targetImageSize, targetImageSize, Image.SCALE_SMOOTH))));
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}

		/**
		 * Assign Tile Color
		 * Set color for this tile.
		 */
		private void AssignTileColor()
		{
			if (BoardUtilities.EIGHTH_RANK[m_TileID] ||
					BoardUtilities.SIXTH_RANK[m_TileID] ||
					BoardUtilities.FOURTH_RANK[m_TileID] ||
					BoardUtilities.SECOND_RANK[m_TileID])
			{
				setBackground(((m_TileID % 2) == 0) ? LIGHT_TILE_COLOR : DARK_TILE_COLOR);
			} else if (BoardUtilities.SEVENTH_RANK[m_TileID] ||
					BoardUtilities.FIFTH_RANK[m_TileID] ||
					BoardUtilities.THIRD_RANK[m_TileID] ||
					BoardUtilities.FIRST_RANK[m_TileID])
			{
				setBackground(((m_TileID % 2) != 0) ? LIGHT_TILE_COLOR : DARK_TILE_COLOR);
			}
		}

		/**
		 * Draw Tile
		 * Draw the tile after set color/icon/highlight.
		 */
		public void DrawTile(Board board)
		{
			AssignTileColor();
			AssignTileIcon(board);
			HighlightTileBorder(m_ChessBoard);
			HighlightLegalTile(m_ChessBoard);
			validate();
			repaint();
		}

		/**
		 * Highlight Legal Tile
		 * Show possible move for the piece that the player has clicked (if enables).
		 */
		private void HighlightLegalTile(final Board board)
		{
			if(m_HighlightLegals)
			{
				for(final Move move : GetLegalMovesOfCurrentChoosingPiece(board))
				{
					if(move.GetDestinationCoordinate() == m_TileID)
					{
						try
						{
							if(move.GetMovedPiece().GetPieceType() == PieceType.KING)
							{
								final MovePreview movePreview = board.GetCurrentPlayer().MakeMove(move);
								if(movePreview.GetMoveStatus() == MoveStatus.LEAVES_PLAYER_IN_CHECK)
								{
									continue;
								}
							}
							if(!move.IsAttackMove())
							{

								ImageIcon image = new ImageIcon(ImageIO.read(new File("import/legalMoves.png")));
								ImageIcon newI = new ImageIcon(image.getImage().getScaledInstance(40, 40, Image.SCALE_SMOOTH));
								add(new JLabel(newI));
							}
							else
							{
								int targetThickness = 2 * (m_WindowFrame.getBounds().height / WINDOW_DIMENSION.height) > 4 ? 4 : 2 * (m_WindowFrame.getBounds().height / WINDOW_DIMENSION.height);

								m_BoardPanel.m_BoardTiles.get(move.GetDestinationCoordinate()).setBorder(BorderFactory.createLineBorder(Color.red,targetThickness));
							}
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
					}
				}
			}
		}

		/**
		 * Highlight Tile Border
		 * Show border around the piece player clicked and the piece can be attacked.
		 */
		private void HighlightTileBorder(final Board board)
		{
			int targetThickness = 2 * (m_WindowFrame.getBounds().width / WINDOW_DIMENSION.width) > 4 ? 4 : 2 * (m_WindowFrame.getBounds().width / WINDOW_DIMENSION.width);
			if(m_PieceChosenByPlayer != null &&
					m_PieceChosenByPlayer.GetAlliance() == board.GetCurrentPlayer().GetAlliance() &&
					m_PieceChosenByPlayer.GetCurrentCoordinate() == m_TileID)
			{
				setBorder(BorderFactory.createLineBorder(Color.cyan,targetThickness));
			}
			else
			{
				setBorder(BorderFactory.createLineBorder(Color.gray));
			}

		}

		/**
		 * Get Legal Moves Of Current Choosing Piece
		 * Returns as the name goes.
		 */
		private Collection<Move> GetLegalMovesOfCurrentChoosingPiece(final Board board)
		{
			if(m_PieceChosenByPlayer != null && m_PieceChosenByPlayer.GetAlliance() == board.GetCurrentPlayer().GetAlliance())
			{
				return m_PieceChosenByPlayer.CalculateLegalMoves(board);
			}
			return Collections.emptyList();
		}
	}

	/*************************************************************************
	 * MoveLog class consists of the moves that player has made
	 * "m_MoveList": The list we're talking about.
	 */
	public static class MoveLog {

		private final ArrayList<Move> m_MoveList;

		MoveLog() {
			m_MoveList = new ArrayList<>();
		}

		public List<Move> GetMoveList() {
			return m_MoveList;
		}

		public void AddMove(final Move move) {
			m_MoveList.add(move);
		}

		public int Size() {
			return m_MoveList.size();
		}

		public void Clear() {
			m_MoveList.clear();
		}

		public Move RemoveMove(int index) {
			return m_MoveList.remove(index);
		}

		public boolean RemoveMove(final Move move) {
			return m_MoveList.remove(move);
		}

	}

	/*************************************************************************
	 * BoardDirection enum represents type of board player is using.
	 */
	public enum BoardDirection
	{
		NORMAL
				{
					@Override
					public List<TilePanel> Traverse(List<TilePanel> boardTiles)
					{
						return boardTiles;
					}

					@Override
					public BoardDirection GetOpposite()
					{
						return BoardDirection.FLIPPED;
					}

					@Override
					public String toString()
					{
						return "NORMAL";
					}
				},
		FLIPPED
				{
					@Override
					public List<TilePanel> Traverse(List<TilePanel> boardTiles)
					{
						// Need to use guava's reverse because we can't just simply clone the list.
						return Lists.reverse(boardTiles);
					}

					@Override
					public BoardDirection GetOpposite()
					{
						return BoardDirection.NORMAL;
					}

					@Override
					public String toString()
					{
						return "FLIPPED";
					}
				};

		/**
		 * Traverse
		 * Return the boardPanel bases of the direction player chose.
		 */
		public abstract List<TilePanel> Traverse(final List<TilePanel> boardTiles);
		public abstract BoardDirection GetOpposite();
	}

	//////////////////////////////// SUB - CLASS ///////////////////////////////////////

}

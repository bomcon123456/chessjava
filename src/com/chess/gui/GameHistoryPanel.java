package com.chess.gui;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.Move;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static com.chess.gui.Window.*;

/*************************************************************************
 * GameHistoryPanel class is the panel show moves that player has made.
 * "m_Table": The pre-made data table.
 * "m_ScrollPane": Pane used when too many moves have been made.
 */
class GameHistoryPanel extends JPanel
{
	private static final Dimension HISTORY_PANEL_DIMENSION = new Dimension(120,40);

	private final DataModel m_Table;
	private final JScrollPane m_ScrollPane;

	GameHistoryPanel()
	{
		this.setLayout(new BorderLayout());
		m_Table = new DataModel();
		final JTable table = new JTable(m_Table);
		table.setRowHeight(15);
		m_ScrollPane = new JScrollPane(table);
		m_ScrollPane.setColumnHeaderView(table.getTableHeader());
		m_ScrollPane.setPreferredSize(HISTORY_PANEL_DIMENSION);
		this.add(m_ScrollPane, BorderLayout.CENTER);
		this.setVisible(true);
	}

	public void redo(final Board board, final MoveLog moveHistory)
	{

		int curRow = 0, count = 0;
		m_Table.Clear();
		for(final Move move : moveHistory.GetMoveList())
		{
			final String moveStr = move.toString();
			if(move.GetMovedPiece().GetAlliance() == Alliance.WHITE)
			{
				if(count == moveHistory.GetMoveList().size()-1)
				{
					m_Table.setValueAt(moveStr + NotationIfInSpecialCases(board), curRow, 0);
				}
				else
				{
					m_Table.setValueAt(moveStr, curRow, 0);
				}
			}
			else if(move.GetMovedPiece().GetAlliance() == Alliance.BLACK)
			{
				if(count == moveHistory.GetMoveList().size()-1)
				{
					m_Table.setValueAt(moveStr + NotationIfInSpecialCases(board), curRow, 1);
				}
				else
				{
					m_Table.setValueAt(moveStr, curRow, 1);
				}
				curRow++;
			}
			count++;
		}
		final JScrollBar vertical = m_ScrollPane.getVerticalScrollBar();
		vertical.setValue(vertical.getMaximum());

		m_ScrollPane.updateUI();
	}

	private String NotationIfInSpecialCases(final Board board)
	{
		if(board.GetCurrentPlayer().IsInCheckMate()) {
			return "(#)";
		} else if(board.GetCurrentPlayer().IsInCheck()) {
			return "(+)";
		}
		return "";

	}

	/*************************************************************************
	 * Row class represents a row of the data table should have what information
	 * "m_xMove": The move that x has made.
	 */
	private static final class Row
	{
		private String m_WhiteMove;
		private String m_BlackMove;

		Row()
		{
		}

		public String GetWhiteMove()
		{
			return m_WhiteMove;
		}

		public String GetBlackMove()
		{
			return m_BlackMove;
		}

		public void SetWhiteMove(final String move)
		{
			m_WhiteMove = move;
		}

		public void SetBlackMove(final String move)
		{
			m_BlackMove = move;
		}
	}

	/*************************************************************************
	 * Data Model used for JPanel.
	 */
	private static final class DataModel extends DefaultTableModel
	{
		private static final String[] COLUMNS_NAME = {"White", "Black"};

		private final List<Row> m_RowValues;

		DataModel()
		{
			m_RowValues = new ArrayList<>();
		}

		public void Clear()
		{
			m_RowValues.clear();
			setRowCount(0);
		}

		@Override
		public int getRowCount()
		{
			if(m_RowValues == null)
			{
				return 0;
			}
			return m_RowValues.size();
		}

		@Override
		public int getColumnCount()
		{
			return COLUMNS_NAME.length;
		}

		@Override
		public Object getValueAt(int row, int column)
		{
			if(row > m_RowValues.size()) return null;
			final Row curRow = m_RowValues.get(row);
			if(column == 0)
			{
				return curRow.GetWhiteMove();
			}
			else if(column == 1)
			{
				return curRow.GetBlackMove();
			}
			return null;
		}

		@Override
		public void setValueAt(Object aValue, int row, int column)
		{
			final Row curRow;
			if (row > m_RowValues.size())
			{
				return;
			}
			else if (row == m_RowValues.size())
			{
				curRow = new Row();
				m_RowValues.add(curRow);
			}
			else
			{
				curRow = m_RowValues.get(row);
			}
			// First time we create this row
			if(column == 0)
			{
				curRow.SetWhiteMove((String)aValue);
				// Because this is the first time <=> we'll insert it to the table
				fireTableRowsInserted(row, row);
			}
			else if(column == 1)
			{
				curRow.SetBlackMove((String)aValue);
				// We only update this time.
				fireTableCellUpdated(row, column);
			}
		}

		@Override
		public Class<?> getColumnClass(int columnIndex)
		{
			return Move.class;
		}

		@Override
		public String getColumnName(int column)
		{
			return COLUMNS_NAME[column];
		}
	}
}

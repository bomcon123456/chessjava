package com.chess.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EtchedBorder;

import com.chess.game.Alliance;
import com.chess.game.Board.Move;
import com.chess.game.Piece.Piece;
import com.google.common.primitives.Ints;

import static com.chess.gui.Window.*;

/*************************************************************************
 * TakenPiecesPanel class is the panel show pieces that player has taken.
 * "m_BlackPanel": Panel consists pieces that the Black Player has taken.
 * "m_WhitePanel": Panel consists pieces that the White Player has taken.
 * "m_Frame": The frame consisting this panel.
 */
class TakenPiecesPanel extends JPanel
{

	private final JPanel m_BlackPanel;
	private final JPanel m_WhitePanel;
	private final JFrame m_Frame;

	private static final Color PANEL_COLOR = Color.decode("0xFDF5E6");
	private static final Dimension TAKEN_PIECES_PANEL_DIMENSION = new Dimension(80, 80);
	private static final EtchedBorder PANEL_BORDER = new EtchedBorder(EtchedBorder.RAISED);


	/**
	 * Constructor
	 * Initialize the properties.
	 */
	public TakenPiecesPanel(JFrame frame)
	{
		super(new BorderLayout());
		setBackground(Color.decode("0xFDF5E6"));
		setBorder(PANEL_BORDER);

		m_BlackPanel = new JPanel(new GridLayout(8, 2, 10,5));
		m_WhitePanel = new JPanel(new GridLayout(8, 2, 10, 5));
		m_BlackPanel.setBackground(PANEL_COLOR);
		m_WhitePanel.setBackground(PANEL_COLOR);
		m_BlackPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		m_WhitePanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);

		add(m_BlackPanel, BorderLayout.NORTH);
		add(m_WhitePanel, BorderLayout.SOUTH);
		setPreferredSize(TAKEN_PIECES_PANEL_DIMENSION);
		m_Frame = frame;
	}

	public void ReDraw(final MoveLog moveLog)
	{
		m_WhitePanel.removeAll();
		m_BlackPanel.removeAll();


		final List<Piece> whiteTakenPieces = new ArrayList<>();
		final List<Piece> blackTakenPieces = new ArrayList<>();

		for(final Move move : moveLog.GetMoveList())
		{
			if(move.IsAttackMove())
			{
				final Piece takenPiece = move.GetAttackedPiece();
				if(takenPiece.GetAlliance() == Alliance.WHITE)
				{
					whiteTakenPieces.add(takenPiece);
				}
				else if(takenPiece.GetAlliance() == Alliance.BLACK)
				{
					blackTakenPieces.add(takenPiece);
				}
				else
				{
					throw new RuntimeException("Should not reach here!");
				}
			}
		}

		Collections.sort(whiteTakenPieces, new Comparator<Piece>()
		{
			@Override
			public int compare(final Piece p1, final Piece p2)
			{
				return Ints.compare(Character.getNumericValue(p1.GetPieceType().toString().charAt(0)), Character.getNumericValue(p2.GetPieceType().toString().charAt(0)));
			}
		});

		Collections.sort(blackTakenPieces, new Comparator<Piece>()
		{
			@Override
			public int compare(final Piece p1, final Piece p2)
			{
				return Ints.compare(Character.getNumericValue(p1.GetPieceType().toString().charAt(0)), Character.getNumericValue(p2.GetPieceType().toString().charAt(0)));
			}
		});

		int targetSizeImage = 25 * (m_Frame.getBounds().width / 600) > 50 ? 50 : 25 * (m_Frame.getBounds().width / 600);
		int count = 0;
		for (final Piece takenPiece : whiteTakenPieces)
		{
			try
			{
				final BufferedImage image = ImageIO.read(new File("import/"
						+ takenPiece.GetAlliance().toString().substring(0, 1) + takenPiece.toString()
						+ ".png"));
				final ImageIcon imageIcon = new ImageIcon(image);
				JLabel test = new JLabel();
				m_WhitePanel.add(new JLabel(new ImageIcon(imageIcon.getImage().getScaledInstance(targetSizeImage,targetSizeImage, Image.SCALE_SMOOTH))));
			}
			catch (final IOException e)
			{
				e.printStackTrace();
			}
			count++;
		}
		if(count == 9)
		{
			m_WhitePanel.updateUI();
		}
		count = 0;
		for (final Piece takenPiece : blackTakenPieces)
		{
			try
			{
				final BufferedImage image = ImageIO.read(new File("import/"
						+ takenPiece.GetAlliance().toString().substring(0, 1) + takenPiece.toString()
						+ ".png"));
				final ImageIcon imageIcon = new ImageIcon(image);

				m_BlackPanel.add(new JLabel(new ImageIcon(imageIcon.getImage().getScaledInstance(targetSizeImage,targetSizeImage, Image.SCALE_SMOOTH))),count);

			} catch (final IOException e) {
				e.printStackTrace();
			}
			count++;
		}
		if(count == 9)
		{
			m_BlackPanel.updateUI();
		}

		validate();
	}
}
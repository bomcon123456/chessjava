package com.chess.game;

import com.chess.game.Board.BoardUtilities;
import com.chess.game.Player.BlackPlayer;
import com.chess.game.Player.Player;
import com.chess.game.Player.WhitePlayer;

/*************************************************************************
 * Alliance represents the two teams in chess game
 */
public enum Alliance
{
	WHITE
	{
		@Override
		public Player ChoosePlayer(final WhitePlayer whitePlayer, final BlackPlayer blackPlayer)
		{
			return whitePlayer;
		}

		@Override
		public boolean IsInPromotionSquare(final int pos) {
			return BoardUtilities.EIGHTH_RANK[pos];
		}
	}
	,
	BLACK
	{
		@Override
		public Player ChoosePlayer(final WhitePlayer whitePlayer, final BlackPlayer blackPlayer)
		{
			return blackPlayer;
		}

		@Override
		public boolean IsInPromotionSquare(final int pos) {
			return BoardUtilities.FIRST_RANK[pos];
		}
	};

	public static int GetDirection(final Alliance alliance)
	{
		return alliance == Alliance.WHITE ? -1 : 1;
	}
	public static int GetOppositeDirection(final Alliance alliance)
	{
		return alliance == Alliance.WHITE ? 1 : -1;
	}

	public abstract Player ChoosePlayer(final WhitePlayer m_whitePlayer, final BlackPlayer m_blackPlayer);
	public abstract boolean IsInPromotionSquare(final int pos);
}

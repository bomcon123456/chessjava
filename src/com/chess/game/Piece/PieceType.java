package com.chess.game.Piece;

/*************************************************************************
 * Piece Type enum
 */
public enum PieceType
{

	PAWN("P"),
	KNIGHT("N"),
	BISHOP("B"),
	ROOK("R"),
	QUEEN("Q"),
	KING("K");

	private final String m_pieceName;


	@Override
	public String toString() {
		return m_pieceName;
	}

	PieceType(final String pieceName) {
		m_pieceName = pieceName;
	}
}

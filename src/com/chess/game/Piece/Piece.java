package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.Move;

import java.util.Collection;

/*************************************************************************
 * Piece Class is the base class for all pieces of Chess.
 * "m_Alliance": pieces's alliance.
 * "m_PieceType":  piece's piece type.
 * "m_CurrentCoordinate": tileID of the tile this piece is on.
 * "m_IsFirstMove": Has this piece moved before?
 * "m_cachedHashCode": We create a cached hash code so that we can access it later without recalculate it.
 */
public abstract class Piece
{
	private final Alliance m_Alliance;
	private final PieceType m_PieceType;
	private int m_CurrentCoordinate;     // ~ TileID
	private boolean m_IsFirstMove;
	private int m_cachedHashCode;

	Piece(final Alliance alliance, final int pos, final PieceType type, final boolean isFirstMove)
	{
		m_CurrentCoordinate = pos;
		m_Alliance = alliance;
		m_PieceType = type;

		m_IsFirstMove = isFirstMove;
		m_cachedHashCode = ComputeHashCode();
	}

	private int ComputeHashCode()
	{
		int result = m_PieceType.hashCode();
		result = 31 * result + m_Alliance.hashCode();
		result = 31 * result + m_CurrentCoordinate;
		result = 31 * result + (m_IsFirstMove ? 1 : 0);
		return result;
	}

	public final int GetCurrentCoordinate()
	{
		return m_CurrentCoordinate;
	}

	public final Alliance GetAlliance()
	{
		return m_Alliance;
	}

	public final PieceType GetPieceType()
	{
		return m_PieceType;
	}

	public final boolean GetIsFirstMove()
	{
		return m_IsFirstMove;
	}

	public final void SetIsFirstMove(boolean b)
	{
		m_IsFirstMove = b;
	}

	/**
	 * Calculate Legal Moves abstract function
	 * With a board, returns all the possible move of this piece.
	 */
	public abstract Collection<Move> CalculateLegalMoves(final Board board);

	/**
	 * MovePiece
	 * Return the piece after the "move" is made.
	 */
	public abstract Piece MovePiece(final Move move);

	@Override
	public boolean equals(final Object other)
	{
		if(this == other)
		{
			return true;
		}
		if(!(other instanceof Piece))
		{
			return false;
		}
		final Piece otherPiece = (Piece)other;
		return ((otherPiece.GetCurrentCoordinate() == m_CurrentCoordinate) &&
				(otherPiece.GetAlliance() == m_Alliance) &&
				(otherPiece.GetPieceType() == m_PieceType) &&
				(otherPiece.m_IsFirstMove == m_IsFirstMove));
	}

	@Override
	public int hashCode()
	{
		return m_cachedHashCode;
	}

	@Override
	public String toString() {
		return "[WARNING] Abstract Piece Should Not Have Been Called.";
	}

}


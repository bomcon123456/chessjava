package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class Rook extends Piece
{
	private final static int[] CANDIDATE_OFFSET = { -8, -1, 1, 8 };

	public Rook(final Alliance alliance, final int pos)
	{
		super(alliance, pos, PieceType.ROOK, true);
	}

	public Rook(final Alliance alliance, final int pos, final boolean isFirstMove)
	{
		super(alliance, pos, PieceType.ROOK, isFirstMove);
	}

	@Override
	public Collection<Move> CalculateLegalMoves(final Board board)
	{
		final List<Move> result = new ArrayList<>();
		for(int currentOffset : CANDIDATE_OFFSET)
		{
			int currentPositionCoord = this.GetCurrentCoordinate();
			while(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
			{
				if(IsInSpecialCases(currentPositionCoord, currentOffset))
				{
					break;
				}
				currentPositionCoord += currentOffset;
				if(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
				{
					final Tile currentTile = board.GetTile(currentPositionCoord);
					if(!currentTile.IsTileOccupied())
					{
						result.add(new NormalMove(board, this, currentPositionCoord));
					}
					else
					{
						final Piece attackedPiece = currentTile.GetPiece();
						if(attackedPiece.GetAlliance() != this.GetAlliance())
						{
							result.add(new NormalAttackMove(board, this, attackedPiece, currentPositionCoord));
						}
						// Can't move further because this piece is blocked by the attackedPiece, so we will break.
						break;
					}
				}

			}



		}
		return result;
	}

	private static boolean IsInSpecialCases(final int currentPositionCoord, final int currentOffset) {
		return (BoardUtilities.FIRST_COLUMN[currentPositionCoord] && (currentOffset == -1) ||
				(BoardUtilities.EIGHTH_COLUMN[currentPositionCoord] && (currentOffset == 1)) );
	}

	@Override
	public Rook MovePiece(final Move move)
	{
		return new Rook(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate());
	}


	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}


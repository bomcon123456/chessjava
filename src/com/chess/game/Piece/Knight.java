package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class Knight extends Piece
{
	private final static int[] CANDIDATE_OFFSET = {-17, -15, -10, -6, 6, 10, 15, 17};

	public Knight(final Alliance alliance, final int position)
	{
		super(alliance, position, PieceType.KNIGHT, true);
	}
	public Knight(final Alliance alliance, final int pos, final boolean firstMove)
	{
		super(alliance, pos, PieceType.KNIGHT,firstMove);
	}

	private static boolean IsInSpecialCases(final int currentPositionCoord, final int currentOffset)
	{
		return (IsFirstColumnExclusion(currentPositionCoord, currentOffset) ||
				IsSecondColumnExclusion(currentPositionCoord, currentOffset) ||
				IsSeventhColumnExclusion(currentPositionCoord, currentOffset) ||
				IsEighthColumnExclusion(currentPositionCoord, currentOffset));
	}

	private static boolean IsFirstColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.FIRST_COLUMN[currentPositionCoord] &&
				((currentOffset == -17) || (currentOffset == -10) ||
						(currentOffset == 6) || (currentOffset == 15));
	}

	private static boolean IsSecondColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.SECOND_COLUMN[currentPositionCoord] && ((currentOffset == -10) || (currentOffset == 6));
	}

	private static boolean IsSeventhColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.SEVENTH_COLUMN[currentPositionCoord] && ((currentOffset == -6) || (currentOffset == 10));
	}

	private static boolean IsEighthColumnExclusion(final int currentPositionCoord,
												   final int currentOffset)
	{
		return BoardUtilities.EIGHTH_COLUMN[currentPositionCoord] &&
				((currentOffset == -15) || (currentOffset == -6) ||
						(currentOffset == 10) || (currentOffset == 17));
	}

	@Override
	public Collection<Move> CalculateLegalMoves(final Board board)
	{
		final List<Move> result = new ArrayList<>();

		for (int currentOffset : CANDIDATE_OFFSET)
		{
			int currentPositionCoord = this.GetCurrentCoordinate();
			if (IsInSpecialCases(currentPositionCoord, currentOffset))
			{
				continue;
			}
			currentPositionCoord += currentOffset;
			if(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
			{
				final Tile currentTile = board.GetTile(currentPositionCoord);
				if (!currentTile.IsTileOccupied())
				{
					result.add(new NormalMove(board, this, currentPositionCoord));
				}
				else
				{
					final Piece attackedPiece = currentTile.GetPiece();
					final Alliance attackedAlliance = attackedPiece.GetAlliance();
					if (this.GetAlliance() != attackedAlliance)
					{
						result.add(new NormalAttackMove(board, this, attackedPiece, currentPositionCoord));
					}
				}
			}

		}

		return result;
	}

	@Override
	public Knight MovePiece(final Move move)
	{
		return new Knight(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate(),false);
	}


	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}

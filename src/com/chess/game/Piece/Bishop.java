package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class Bishop extends Piece
{
	private final static int[] CANDIDATE_OFFSET = {-9, -7, 7, 9};

	public Bishop(final Alliance alliance, final int pos)
	{
		super(alliance, pos, PieceType.BISHOP,true);
	}
	public Bishop(final Alliance alliance, final int pos, final boolean firstMove)
	{
		super(alliance, pos, PieceType.BISHOP,firstMove);
	}

	@Override
	public Collection<Move> CalculateLegalMoves(final Board board)
	{
		final List<Move> result = new ArrayList<>();
		for(int currentOffset : CANDIDATE_OFFSET)
		{
			int currentPositionCoord = this.GetCurrentCoordinate();
			while(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
			{
				if(IsInSpecialCases(currentPositionCoord, currentOffset))
				{
					break;
				}
				currentPositionCoord += currentOffset;
				if(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
				{
					final Tile currentTile = board.GetTile(currentPositionCoord);
					if(!currentTile.IsTileOccupied())
					{
						result.add(new NormalMove(board, this, currentPositionCoord));
					}
					else
					{
						final Piece attackedPiece = currentTile.GetPiece();
						if(attackedPiece.GetAlliance() != this.GetAlliance())
						{
							result.add(new NormalAttackMove(board, this, attackedPiece, currentPositionCoord));
						}
						// Can't move further because this piece is blocked by the attackedPiece, so we will break.
						break;
					}
				}
			}
		}
		return result;
	}

	@Override
	public Bishop MovePiece(final Move move)
	{
		return new Bishop(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate());
	}

	private static boolean IsInSpecialCases(final int currentPositionCoord, final int currentOffset) {
		return (IsFirstColumnExclusion(currentPositionCoord, currentOffset) || IsEighthColumnExclusion(currentPositionCoord, currentOffset));
	}

	private static boolean IsFirstColumnExclusion(final int currentPositionCoord, final int currentOffset) {
		return (BoardUtilities.FIRST_COLUMN[currentPositionCoord] &&
				((currentOffset == -9) || (currentOffset == 7)));
	}

	private static boolean IsEighthColumnExclusion(final int currentPositionCoord, final int currentOffset) {
		return BoardUtilities.EIGHTH_COLUMN[currentPositionCoord] &&
				((currentOffset == -7) || (currentOffset == 9));
	}

	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}

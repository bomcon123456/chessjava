package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class King extends Piece
{
	private final static int[] CANDIDATE_OFFSET = { -9, -8, -7, -1, 1, 7, 8, 9 };

	public King(final Alliance alliance, final int pos)
	{
		super(alliance, pos, PieceType.KING, true);
	}
	public King(final Alliance alliance, final int pos, final boolean firstMove)
	{
		super(alliance, pos, PieceType.KING,firstMove);
	}

	@Override
	public Collection<Move> CalculateLegalMoves(Board board)
	{
		final List<Move> result = new ArrayList<>();
		for(int currentOffset : CANDIDATE_OFFSET)
		{
			int currentPosition = this.GetCurrentCoordinate();
			if(IsInSpecialCases(currentPosition, currentOffset))
			{
				continue;
			}
			currentPosition += currentOffset;
			if(BoardUtilities.IsCoordinateInBound(currentPosition))
			{
				final Tile currentTile = board.GetTile(currentPosition);
				if(!currentTile.IsTileOccupied())
				{
					result.add(new NormalMove(board, this, currentPosition));
				}
				else
				{
					final Piece attackedPiece = currentTile.GetPiece();
					if(attackedPiece.GetAlliance() != this.GetAlliance())
					{
						result.add(new NormalAttackMove(board, this, attackedPiece, currentPosition));
					}
				}
			}
		}
		if(board.GetCurrentPlayer() != null)
		{
			for(final Move move : board.GetCurrentPlayer().GetLegalMoves())
			{
				if(move.GetMovedPiece().equals(this))
				{
					if(move.IsCastlingMove())
					{
						result.add(move);
					}
				}
			}
		}

		return result;
	}

	private static boolean IsInSpecialCases(final int currentPositionCoord, final int currentOffset)
	{
		return (IsFirstColumnExclusion(currentPositionCoord, currentOffset) || IsEighthColumnExclusion(currentPositionCoord, currentOffset));
	}

	private static boolean IsFirstColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.FIRST_COLUMN[currentPositionCoord]
				&& ((currentOffset == -9) || (currentOffset == -1) ||
				(currentOffset == 7));
	}

	private static boolean IsEighthColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.EIGHTH_COLUMN[currentPositionCoord]
				&& ((currentOffset == -7) || (currentOffset == 1) ||
				(currentOffset == 9));
	}

	@Override
	public King MovePiece(final Move move)
	{
		return new King(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate(), this.GetIsFirstMove());
	}


	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}

package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class Queen extends Piece
{
	private final static int[] CANDIDATE_OFFSET = { -9, -8, -7, -1, 1, 7, 8, 9 };

	public Queen(final Alliance alliance, final int pos)
	{
		super(alliance, pos, PieceType.QUEEN, true);
	}
	public Queen(final Alliance alliance, final int pos, final boolean isFirstMove)
	{
		super(alliance, pos, PieceType.QUEEN, isFirstMove);
	}

	@Override
	public Collection<Move> CalculateLegalMoves(Board board)
	{
		final List<Move> result = new ArrayList<>();
		for(int currentOffset : CANDIDATE_OFFSET)
		{
			int currentPositionCoord = this.GetCurrentCoordinate();
			while(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
			{
				if(IsInSpecialCases(currentPositionCoord, currentOffset))
				{
					break;
				}
				currentPositionCoord += currentOffset;
				if(BoardUtilities.IsCoordinateInBound(currentPositionCoord))
				{
					final Tile currentTile = board.GetTile(currentPositionCoord);
					if(!currentTile.IsTileOccupied())
					{
						result.add(new NormalMove(board, this, currentPositionCoord));
					}
					else
					{
						final Piece attackedPiece = currentTile.GetPiece();
						if(attackedPiece.GetAlliance() != this.GetAlliance())
						{
							result.add(new NormalAttackMove(board, this, attackedPiece, currentPositionCoord));
						}
						// Can't move further because this piece is blocked by the attackedPiece, so we will break.
						break;
					}
				}
			}
		}
		return result;
	}

	private static boolean IsInSpecialCases(final int currentPositionCoord, final int currentOffset)
	{
		return (IsFirstColumnExclusion(currentPositionCoord, currentOffset) || IsEightColumnExclusion(currentPositionCoord, currentOffset));
	}

	private static boolean IsFirstColumnExclusion(final int currentPositionCoord, final int currentOffset)
	{
		return BoardUtilities.FIRST_COLUMN[currentPositionCoord] && ((currentOffset == -9)
				|| (currentOffset == -1) || (currentOffset == 7));
	}

	private static boolean IsEightColumnExclusion(final int currentPositionCoord, final int currentOffset) {
		return BoardUtilities.EIGHTH_COLUMN[currentPositionCoord] && ((currentOffset == -7)
				|| (currentOffset == 1) || (currentOffset == 9));
	}

	@Override
	public Queen MovePiece(final Move move)
	{
		return new Queen(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate(),false);
	}


	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}

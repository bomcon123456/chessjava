package com.chess.game.Piece;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.BoardUtilities;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class Pawn extends Piece
{
	private final static int[] CANDIDATE_OFFSET = {8, 16, 7, 9};

	public Pawn(final Alliance alliance, final int pos)
	{
		super(alliance, pos, PieceType.PAWN, true);
	}

	public Pawn(final Alliance alliance, final int pos, final boolean firstMove)
	{
		super(alliance, pos, PieceType.PAWN,firstMove);
	}

	@Override
	public Collection<Move> CalculateLegalMoves(Board board)
	{
		final List<Move> result = new ArrayList<>();

		for (int currentOffset : CANDIDATE_OFFSET)
		{
			int destinationPos = this.GetCurrentCoordinate() + (Alliance.GetDirection(this.GetAlliance()) * currentOffset);

			if(!BoardUtilities.IsCoordinateInBound(destinationPos))
			{
				continue;
			}
			if(IsAbleToMoveOneSquareForward(board, currentOffset, destinationPos))
			{
				if(GetAlliance().IsInPromotionSquare(destinationPos))
				{
					result.add(new PawnPromotionMove(new PawnMove(board,this, destinationPos)));
				}
				else
				{
					result.add(new PawnMove(board, this, destinationPos));
				}
			}
			else if(IsAbleToJump(board, this.GetAlliance(), this.GetCurrentCoordinate(), currentOffset))
			{
				result.add(new PawnJump(board, this, destinationPos));
			}
			else if(IsInFirstSpecialAttackCase(currentOffset, this.GetCurrentCoordinate(), this.GetAlliance()))
			{
				final Tile destinationTile = board.GetTile(destinationPos);
				if(destinationTile.IsTileOccupied())
				{
					final Piece piece = destinationTile.GetPiece();
					if(this.GetAlliance() != piece.GetAlliance())
					{
						if(GetAlliance().IsInPromotionSquare(destinationPos))
						{
							result.add(new PawnPromotionMove(new PawnAttackMove(board, this, piece, destinationPos)));
						}
						else
						{
							result.add(new PawnAttackMove(board, this, piece, destinationPos));
						}
					}
				}
				else if(board.GetEnPassantPawn() != null)
				{
					final Pawn thePawn = board.GetEnPassantPawn();
					// This mean thePawn is on the current pawn's right (if white).
					if(thePawn.GetCurrentCoordinate() == (this.GetCurrentCoordinate() + Alliance.GetOppositeDirection(this.GetAlliance())))
					{
						if(thePawn.GetAlliance() != this.GetAlliance())
						{
							result.add(new PawnEnPassantAttackMove(board, this, thePawn, destinationPos));
						}
					}
				}
			}
			else if(IsInSecondSpecialAttackCase(currentOffset, this.GetCurrentCoordinate(), this.GetAlliance()))
			{
				final Tile destinationTile = board.GetTile(destinationPos);
				if(destinationTile.IsTileOccupied())
				{
					final Piece piece = destinationTile.GetPiece();
					if(this.GetAlliance() != piece.GetAlliance())
					{
						if(GetAlliance().IsInPromotionSquare(destinationPos))
						{
							result.add(new PawnPromotionMove(new PawnAttackMove(board, this, piece, destinationPos)));
						}
						else
						{
							result.add(new PawnAttackMove(board, this, piece, destinationPos));
						}					}
				}
				else if(board.GetEnPassantPawn() != null)
				{
					final Pawn thePawn = board.GetEnPassantPawn();
					// This mean thePawn is on the current pawn's right (if white).
					if(thePawn.GetCurrentCoordinate() == (this.GetCurrentCoordinate() + Alliance.GetDirection(this.GetAlliance())))
					{
						if(thePawn.GetAlliance() != this.GetAlliance())
						{
							result.add(new PawnEnPassantAttackMove(board, this, thePawn, destinationPos));
						}
					}
				}
			}
		}
		return result;
	}

	private boolean IsAbleToJump(final Board board, final Alliance alliance, final int currentPosition, final int currentOffset)
	{
		if(currentOffset == 16 && this.GetIsFirstMove() &&
				((BoardUtilities.SEVENTH_RANK[currentPosition] && alliance == Alliance.BLACK) ||
						(BoardUtilities.SECOND_RANK[currentPosition] && alliance == Alliance.WHITE)))
		{
			final int squareBetweenCurrentAndDestinationPos = currentPosition + Alliance.GetDirection(alliance) * 8;
			final int destinationPos = currentPosition + Alliance.GetDirection(alliance) * 16;
			final Tile thatTile = board.GetTile(squareBetweenCurrentAndDestinationPos);
			final Tile destinationTile = board.GetTile(destinationPos);
			if((!thatTile.IsTileOccupied()) && (!destinationTile.IsTileOccupied()))
			{
				return true;
			}
			return false;
		}
		return false;
	}

	private boolean IsAbleToMoveOneSquareForward(final Board board, final int currentOffset, final int destinationPostion)
	{
		return (currentOffset == 8 && !board.GetTile(destinationPostion).IsTileOccupied());
	}

	// This case is when offset == 7 and black and white is respectively in the first / eighth column.
	private static boolean IsInFirstSpecialAttackCase(final int currentOffset, final int currentPosition, final Alliance alliance)
	{
		return (
				currentOffset == 7 &&
					!((BoardUtilities.EIGHTH_COLUMN[currentPosition] && alliance == Alliance.WHITE) ||
						(BoardUtilities.FIRST_COLUMN[currentPosition] && alliance == Alliance.BLACK))
		);
	}

	// This case is when offset == 9 and black and white is respectively in the eighth / first column.
	private static boolean IsInSecondSpecialAttackCase(final int currentOffset, final int currentPosition, final Alliance alliance)
	{
		return (
				currentOffset == 9 &&
					!((BoardUtilities.EIGHTH_COLUMN[currentPosition] && alliance == Alliance.BLACK) ||
					(BoardUtilities.FIRST_COLUMN[currentPosition] && alliance == Alliance.WHITE))
		);
	}

	public Piece GetPromotionPiece()
	{
		return new Queen(this.GetAlliance(), this.GetCurrentCoordinate(), false);
	}

	@Override
	public Pawn MovePiece(final Move move)
	{
		return new Pawn(move.GetMovedPiece().GetAlliance(), move.GetDestinationCoordinate());
	}


	@Override
	public String toString() {
		return this.GetPieceType().toString();
	}
}

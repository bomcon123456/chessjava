package com.chess.game.Player;

import com.chess.game.Alliance;
import com.chess.game.Board.*;
import com.chess.game.Piece.King;
import com.chess.game.Piece.Piece;
import com.chess.game.Piece.PieceType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*************************************************************************
 * Player abstract class is the base class for the player.
 * "m_Board": the board this player is playing.
 * "m_PlayerKing":  this player's king.
 * "m_LegalMoves": all the possible move the player can make with his/her active pieces.
 * "m_IsInChecked": Is this player's king in checked?
 */

public abstract class Player
{
	protected final Board m_Board;
	protected final King m_PlayerKing;
	protected final Collection<Move> m_LegalMoves;
	private boolean m_IsInChecked;

	Player(final Board board, Collection<Move> ourLegalMoves, Collection<Move> opponentLegalMoves)
	{
		m_Board = board;
		m_PlayerKing = FindKingInActiveBoard();
		List<Move> legals = new ArrayList<>();
		legals.addAll(ourLegalMoves);
		legals.addAll(CalculateKingCastleMoves(ourLegalMoves, opponentLegalMoves));
		m_LegalMoves = legals;
		m_IsInChecked = !(Player.CalculateAttacksOnTile(m_PlayerKing.GetCurrentCoordinate(), opponentLegalMoves).isEmpty());
	}

	/**
	 * Calculate Attacks On Tile
	 * Returns all the attack move has destination "pos"
	 */
	protected static Collection<Move> CalculateAttacksOnTile(final int pos, Collection<Move> opponentLegalMoves)
	{
		final List<Move> result = new ArrayList<>();
		for(final Move move : opponentLegalMoves)
		{
			if(pos == move.GetDestinationCoordinate())
			{
				result.add(move);
			}
		}
		return result;
	}

	/**
	 * Returns this player's king.
	 */
	private final King FindKingInActiveBoard()
	{
		for(final Piece currentPiece : GetActivePieces())
		{
			if(currentPiece.GetPieceType() == PieceType.KING)
			{
				return (King)currentPiece;
			}
		}
		throw new RuntimeException("Invalid board! Should've already been ended game now.");
	}

	/**
	 * Check if the king can go anywhere to escape checked.
	 */
	private boolean IsEscapable()
	{
		for(final Move move : m_LegalMoves)
		{
			final MovePreview movePreview = this.MakeMove(move);
			if(movePreview.GetMoveStatus() == MoveStatus.VALID_MOVE_COMPLETE)
			{
				return true;
			}
		}
		return false;
	}

	//////////// PUBLIC FUNC //////////////////
	public final boolean IsMoveLegal(final Move move)
	{
		return m_LegalMoves.contains(move);
	}

	public boolean IsInCheck()
	{
		return m_IsInChecked;
	}

	public boolean IsInCheckMate()
	{
		return (m_IsInChecked) && !(IsEscapable());
	}

	public boolean IsInStaleMate()
	{
		return !(m_IsInChecked) && !(IsEscapable());
	}

	public boolean IsCastled()
	{
		return false;
	}

	public final Collection<Move> GetLegalMoves()
	{
		return m_LegalMoves;
	}

	public final King GetPlayerKing()
	{
		return m_PlayerKing;
	}

	/**
	 * Make Move
	 * Returns MovePreview for the move after checking if this move would make the opponent runs into checked situation.
	 */
	public MovePreview MakeMove(final Move move)
	{
		if(!IsMoveLegal(move))
		{
			return new MovePreview(m_Board, move, MoveStatus.INVALID_MOVE);
		}
		final Board transitionBoard = move.CreateBoardFromMove();

		/*
		 * Because when we CreateBoardFromMove the move, currentPlayer will become the Opponent's of our actual GetCurrentPlayer.
		 */
		final Collection<Move> totalAttacksOnOpponentKing = Player.CalculateAttacksOnTile(transitionBoard.GetCurrentPlayer().GetOpponent().GetPlayerKing().GetCurrentCoordinate(),
																							transitionBoard.GetCurrentPlayer().GetLegalMoves());

		if(!totalAttacksOnOpponentKing.isEmpty())
		{
			return new MovePreview(m_Board, move, MoveStatus.LEAVES_PLAYER_IN_CHECK);
		}
		return new MovePreview(transitionBoard, move, MoveStatus.VALID_MOVE_COMPLETE);
	}

	/**
	 * Make Undo Move
	 * Returns MovePreview for the move after reset firstmove for special cases.
	 */
	public MovePreview MakeUndoMove(final Move move)
	{
		if(move instanceof Move.KingSideCastleMove || move instanceof Move.QueenSideCastleMove ||
				move instanceof Move.PawnJump ||
				((move instanceof Move.PawnMove || move instanceof Move.PawnAttackMove || move instanceof Move.PawnEnPassantAttackMove) &&
						(BoardUtilities.SEVENTH_RANK[move.GetPieceCurrentCoordinate()] ||
								BoardUtilities.SECOND_RANK[move.GetPieceCurrentCoordinate()]))
		)
		{
			move.GetMovedPiece().SetIsFirstMove(true);
		}
		return new MovePreview(move.CreateBoardBeforeThisMove(), move, MoveStatus.VALID_MOVE_COMPLETE);
	}
	//////////// PUBLIC FUNC //////////////////

	//////////// ABSTRACT FUNC ////////////////
	public abstract Collection<Piece> GetActivePieces();
	public abstract Alliance GetAlliance();
	public abstract Player GetOpponent();
	public abstract Collection<Move> CalculateKingCastleMoves(Collection<Move> playerLegals, Collection<Move> opponentLegals);
	//////////// ABSTRACT FUNC ////////////////


}

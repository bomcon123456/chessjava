package com.chess.game.Player;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.Move;
import com.chess.game.Board.Move.KingSideCastleMove;
import com.chess.game.Board.Tile;
import com.chess.game.Piece.Piece;
import com.chess.game.Piece.PieceType;
import com.chess.game.Piece.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class BlackPlayer extends Player
{
	public BlackPlayer(Board board, Collection<Move> ourLegal, Collection<Move> opponentLegal)
	{
		super(board, ourLegal, opponentLegal);
	}

	@Override
	public Collection<Piece> GetActivePieces()
	{
		return m_Board.GetActiveBlackPieces();
	}

	@Override
	public Alliance GetAlliance()
	{
		return Alliance.BLACK;
	}

	@Override
	public Player GetOpponent()
	{
		return m_Board.GetWhitePlayer();
	}

	@Override
	public Collection<Move> CalculateKingCastleMoves(final Collection<Move> playerLegals, final Collection<Move> opponentLegals)
	{
		List<Move> result = new ArrayList<>();

		if(m_PlayerKing.GetIsFirstMove() && !this.IsInCheck())
		{
			// If F8 and G8 is not occupied.
			if (IsSuitableForKingSideCastle(opponentLegals))
			{
				// Because rook is not moved so he must be in the default pos.
				final Tile rookTile = m_Board.GetTile(7);
				if (rookTile.IsTileOccupied() &&
						rookTile.GetPiece().GetIsFirstMove() &&
						rookTile.GetPiece().GetPieceType() == PieceType.ROOK)
				{
					final Rook rookPiece = (Rook) rookTile.GetPiece();
					// King will be at G8 and Rook will be at F8
					result.add(new KingSideCastleMove(m_Board, m_PlayerKing, 6, rookPiece, rookPiece.GetCurrentCoordinate(), 5));
				}
			}
			// If B8/C8/D8 is not occupied.
			if (IsSuitableForQueenSideCastle(opponentLegals))
			{
				// Because rook is not moved so he must be in the default pos.
				final Tile rookTile = m_Board.GetTile(0);
				if (rookTile.IsTileOccupied() &&
						rookTile.GetPiece().GetIsFirstMove() &&
						rookTile.GetPiece().GetPieceType() == PieceType.ROOK)
				{
					final Rook rookPiece = (Rook) rookTile.GetPiece();
					// King will be at C8 and Rook will be at D8
					result.add(new QueenSideCastleMove(m_Board, m_PlayerKing, 2, rookPiece, rookPiece.GetCurrentCoordinate(), 3));
				}
			}
		}
		return result;
	}

	private boolean IsSuitableForKingSideCastle(final Collection<Move> opponentLegals)
	{
		return (!(m_Board.GetTile(5).IsTileOccupied()) &&
				!(m_Board.GetTile(6).IsTileOccupied()) &&
				(Player.CalculateAttacksOnTile(5, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(6, opponentLegals).isEmpty()));
	}

	private boolean IsSuitableForQueenSideCastle(final Collection<Move> opponentLegals)
	{
		return (!(m_Board.GetTile(1).IsTileOccupied()) &&
				!(m_Board.GetTile(2).IsTileOccupied()) &&
				!(m_Board.GetTile(3).IsTileOccupied()) &&
				(Player.CalculateAttacksOnTile(1, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(2, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(3, opponentLegals).isEmpty()));
	}
}

package com.chess.game.Player;

import com.chess.game.Alliance;
import com.chess.game.Board.Board;
import com.chess.game.Board.Move;
import com.chess.game.Board.Tile;
import com.chess.game.Piece.Piece;
import com.chess.game.Piece.PieceType;
import com.chess.game.Piece.Rook;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.chess.game.Board.Move.*;

public class WhitePlayer extends Player
{
	public WhitePlayer(Board board, Collection<Move> ourLegal, Collection<Move> opponentLegal)
	{
		super(board, ourLegal, opponentLegal);
	}

	@Override
	public Collection<Piece> GetActivePieces()
	{
		return m_Board.GetActiveWhitePieces();
	}

	@Override
	public Alliance GetAlliance()
	{
		return Alliance.WHITE;
	}

	@Override
	public Player GetOpponent()
	{
		return m_Board.GetBlackPlayer();
	}

	@Override
	public Collection<Move> CalculateKingCastleMoves(final Collection<Move> playerLegals, final Collection<Move> opponentLegals)
	{
		List<Move> result = new ArrayList<>();

		if(m_PlayerKing.GetIsFirstMove() && !this.IsInCheck())
		{
			// If F1 and G1 is not occupied
			if (IsSuitableForKingSideCastle(opponentLegals))
			{
				// Because rook is not moved so he must be in the default pos.
				final Tile rookTile = m_Board.GetTile(63);
				if (rookTile.IsTileOccupied() &&
					rookTile.GetPiece().GetIsFirstMove() &&
					rookTile.GetPiece().GetPieceType() == PieceType.ROOK)
				{
					final Rook rookPiece = (Rook) rookTile.GetPiece();
					// King will be at G1 and Rook will be at F1
					result.add(new KingSideCastleMove(m_Board, m_PlayerKing, 62, rookPiece, rookPiece.GetCurrentCoordinate(), 61));
				}
			}
			// If B1/C1/D1 is not occupied.
			if (IsSuitableForQueenSideCastle(opponentLegals))
			{
				// Because rook is not moved so he must be in the default pos.
				final Tile rookTile = m_Board.GetTile(56);
				if (rookTile.IsTileOccupied() &&
					rookTile.GetPiece().GetIsFirstMove() &&
					rookTile.GetPiece().GetPieceType() == PieceType.ROOK)
				{
					final Rook rookPiece = (Rook) rookTile.GetPiece();
					// King will be at C1 and Rook will be at D1
					result.add(new QueenSideCastleMove(m_Board, m_PlayerKing, 58, rookPiece, rookPiece.GetCurrentCoordinate(), 59));
				}
			}
		}
		return result;
	}

	private boolean IsSuitableForKingSideCastle(final Collection<Move> opponentLegals)
	{
		return (!(m_Board.GetTile(61).IsTileOccupied()) &&
				!(m_Board.GetTile(62).IsTileOccupied()) &&
				(Player.CalculateAttacksOnTile(61, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(62, opponentLegals).isEmpty()));
	}


	private boolean IsSuitableForQueenSideCastle(final Collection<Move> opponentLegals)
	{
		return (!(m_Board.GetTile(59).IsTileOccupied()) &&
				!(m_Board.GetTile(58).IsTileOccupied()) &&
				!(m_Board.GetTile(57).IsTileOccupied()) &&
				(Player.CalculateAttacksOnTile(59, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(58, opponentLegals).isEmpty()) &&
				(Player.CalculateAttacksOnTile(57, opponentLegals).isEmpty()));
	}
}

package com.chess.game.Board;

import com.chess.game.Alliance;
import com.chess.game.Piece.*;
import com.chess.game.Player.BlackPlayer;
import com.chess.game.Player.Player;
import com.chess.game.Player.WhitePlayer;

import java.util.*;

/*************************************************************************
 * Board Class represents a state of a Chess game. A State will be created every time player makes a new move.
 * Board consists of:
 * - "m_GameBoard": Represent 64 tiles of the Chess's board, also the pieces on those tiles.
 * - "m_xPieces": Contains all active pieces of x player
 * - "m_xPlayer": Instance of the player.
 * - "m_CurrentPlayer": The player will make the next move.
 * - "m_EnPassantPawn": The pawn that just has made a jump.

 */

public class Board
{

	private final List<Tile> m_GameBoard;

	private final List<Piece> m_WhitePieces;
	private final List<Piece> m_BlackPieces;

	private final WhitePlayer m_WhitePlayer;
	private final BlackPlayer m_BlackPlayer;
	private final Player m_CurrentPlayer;

	private final Pawn m_EnPassantPawn;


	////////////////////////////// PRIVATE FUNCTION ///////////////////////////////////
	/**
	 * Board's Constructor
	 * Using Builder Design Pattern
	 * Will be created (called) every time player makes new move, then pass information to the builder
	 * Then builder will make the board.
	 */
	private Board(BoardBuilder boardBuilder)
	{
		m_GameBoard = CreateGameBoard(boardBuilder);
		m_WhitePieces = GetCurrentPiecesOfTeam(Alliance.WHITE);
		m_BlackPieces = GetCurrentPiecesOfTeam(Alliance.BLACK);

		m_EnPassantPawn = boardBuilder.m_EnPassantPawn;

		final Collection<Move> whiteLegalMoves = CalculateLegalMoves(m_WhitePieces);
		final Collection<Move> blackLegalMoves = CalculateLegalMoves(m_BlackPieces);

		m_WhitePlayer = new WhitePlayer(this, whiteLegalMoves, blackLegalMoves);
		m_BlackPlayer = new BlackPlayer(this, blackLegalMoves, whiteLegalMoves);
		m_CurrentPlayer = boardBuilder.m_MoveMaker.ChoosePlayer(m_WhitePlayer, m_BlackPlayer);

	}

	/**
	 * Get all active pieces of a player
	 */
	private List<Piece> GetCurrentPiecesOfTeam(final Alliance alliance)
	{
		List<Piece> result = new ArrayList<>();
		for(Tile currentTile : m_GameBoard)
		{
			if(currentTile.IsTileOccupied() && currentTile.GetPiece() != null)
			{
				if(currentTile.GetPiece().GetAlliance() == alliance)
					result.add(currentTile.GetPiece());
			}
		}
		return result;
	}

	/**
	 * Calculate all possible move that player can make
	 */
	private Collection<Move> CalculateLegalMoves(final Collection<Piece> pieces)
	{
		final List<Move> result = new ArrayList<>();
		for (Piece currentPiece : pieces)
		{
			result.addAll(currentPiece.CalculateLegalMoves(this));
		}
		return result;
	}


	/**
	 * Add 64 tiles to the m_GameBoard (so that m_GameBoard consists those tile and also its information
	 */
	private List<Tile> CreateGameBoard(final BoardBuilder boardBuilder)
	{
		Tile[] result = new Tile[BoardUtilities.NUM_TILES];
		for(int i = 0; i < BoardUtilities.NUM_TILES; i++)
		{
			result[i] = Tile.CreateTile(i, boardBuilder.GetBoardConfig().get(i));
		}
		return Arrays.asList(result);
	}
	////////////////////////////// PRIVATE FUNCTION ///////////////////////////////////

	////////////////////////////// PUBLIC FUNCTION ///////////////////////////////////


	/**
	 * Create Standard Game Board Position
	 */
	public static Board CreateStandardGameBoard()
	{
		BoardBuilder boardBuilder = new BoardBuilder();
		// Black Layout
		boardBuilder.SetPiece(new Rook(Alliance.BLACK, 0));
		boardBuilder.SetPiece(new Knight(Alliance.BLACK, 1));
		boardBuilder.SetPiece(new Bishop(Alliance.BLACK, 2));
		boardBuilder.SetPiece(new Queen(Alliance.BLACK, 3));
		boardBuilder.SetPiece(new King(Alliance.BLACK, 4));
		boardBuilder.SetPiece(new Bishop(Alliance.BLACK, 5));
		boardBuilder.SetPiece(new Knight(Alliance.BLACK, 6));
		boardBuilder.SetPiece(new Rook(Alliance.BLACK, 7));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 8));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 9));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 10));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 11));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 12));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 13));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 14));
		boardBuilder.SetPiece(new Pawn(Alliance.BLACK, 15));
		// White Layout
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 48));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 49));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 50));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 51));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 52));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 53));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 54));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 55));
		boardBuilder.SetPiece(new Rook(Alliance.WHITE, 56));
		boardBuilder.SetPiece(new Knight(Alliance.WHITE, 57));
		boardBuilder.SetPiece(new Bishop(Alliance.WHITE, 58));
		boardBuilder.SetPiece(new Queen(Alliance.WHITE, 59));
		boardBuilder.SetPiece(new King(Alliance.WHITE, 60));
		boardBuilder.SetPiece(new Bishop(Alliance.WHITE, 61));
		boardBuilder.SetPiece(new Knight(Alliance.WHITE, 62));
		boardBuilder.SetPiece(new Rook(Alliance.WHITE, 63));
		//white to move
		boardBuilder.SetMoveMaker(Alliance.WHITE);
		//build the board
		return boardBuilder.Build();

	}

	/**
	 * Create Bug Game Board
	 */
	public static Board CreateKingCaslteBug()
	{
		BoardBuilder boardBuilder = new BoardBuilder();
		// Black Layout
		boardBuilder.SetPiece(new King(Alliance.BLACK, 4));
		// White Layout
		boardBuilder.SetPiece(new Rook(Alliance.WHITE, 56));
		boardBuilder.SetPiece(new Knight(Alliance.WHITE, 50));
		boardBuilder.SetPiece(new Bishop(Alliance.WHITE, 51));
		boardBuilder.SetPiece(new Queen(Alliance.WHITE, 52));
		boardBuilder.SetPiece(new King(Alliance.WHITE, 60));
		boardBuilder.SetPiece(new Bishop(Alliance.WHITE, 53));
		boardBuilder.SetPiece(new Knight(Alliance.WHITE, 54));
		boardBuilder.SetPiece(new Rook(Alliance.WHITE, 63));
		//white to move
		boardBuilder.SetMoveMaker(Alliance.WHITE);
		//build the board
		return boardBuilder.Build();

	}

	/**
	 * Create A Famous Game Board
	 */
	public static Board CreateTwoPawnGameBoard()
	{
		BoardBuilder boardBuilder = new BoardBuilder();
		// Black Layout
		boardBuilder.SetPiece(new King(Alliance.BLACK, 4));

		// White Layout
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 12));
		boardBuilder.SetPiece(new Pawn(Alliance.WHITE, 19));
		boardBuilder.SetPiece(new Queen(Alliance.WHITE, 20));
		boardBuilder.SetPiece(new King(Alliance.WHITE, 60));
		//White to move
		boardBuilder.SetMoveMaker(Alliance.WHITE);
		//build the board
		return boardBuilder.Build();

	}

	/**
	 * Create A Famous Game Board
	 */
	public static Board CreateKingRookGameBoard()
	{
		BoardBuilder boardBuilder = new BoardBuilder();
		// Black Layout
		boardBuilder.SetPiece(new King(Alliance.BLACK, 5));

		// White Layout
		boardBuilder.SetPiece(new Rook(Alliance.WHITE, 8));
		boardBuilder.SetPiece(new King(Alliance.WHITE, 60));
		//Black to move
		boardBuilder.SetMoveMaker(Alliance.BLACK);
		//build the board
		return boardBuilder.Build();

	}

	public Tile GetTile(final int currentPositionCoord)
	{
		return m_GameBoard.get(currentPositionCoord);
	}

	public Collection<Piece> GetActiveWhitePieces()
	{
		return m_WhitePieces;
	}
	public Collection<Piece> GetActiveBlackPieces()
	{
		return m_BlackPieces;
	}
	public Pawn GetEnPassantPawn()
	{
		return m_EnPassantPawn;
	}


	public WhitePlayer GetWhitePlayer() { return m_WhitePlayer; }
	public BlackPlayer GetBlackPlayer() { return m_BlackPlayer; }
	public Player GetCurrentPlayer()
	{
		return m_CurrentPlayer;
	}

	public List<Move> GetAllLegalMoves()
	{
		final List<Move> result = new ArrayList<>();
		result.addAll(CalculateLegalMoves(m_WhitePieces));
		result.addAll(CalculateLegalMoves(m_BlackPieces));
		return result;
	}

	public List<Piece> GetAllActivePieces()
	{
		final List<Piece> result = new ArrayList<>();
		result.addAll(m_WhitePieces);
		result.addAll(m_BlackPieces);
		return result;
	}

	@Override
	public String toString()
	{
		final StringBuilder builder = new StringBuilder();
		for(int i = 0; i < BoardUtilities.NUM_TILES; i++)
		{
			final Tile currentTile = m_GameBoard.get(i);
			String str = "-";
			if(currentTile.GetPiece() != null)
			{
				str = currentTile.GetPiece().toString();
				if(currentTile.GetPiece().GetAlliance() == Alliance.BLACK)
				{
					str = str.toLowerCase();
				}
			}
			str+= " ";
			builder.append(str);
			if((i+1) % 8 == 0)
			{
				builder.append("\n");
			}
		}
		return builder.toString();
	}

	////////////////////////////// PUBLIC FUNCTION ///////////////////////////////////


	/*********************************
	 * BoardBuilder
	 * "m_BoardConfig": Consists of piece's and its m_TileID (coordinate on Board)
	 * "m_MoveMaker": The player will make the next move on this board (current player)
	 * "m_EnPassantPawn": The pawn that just has made a jump.
	 */
	public static class BoardBuilder
	{
		Map<Integer, Piece> m_BoardConfig;
		Alliance m_MoveMaker;
		Pawn m_EnPassantPawn;

		/**
		 * Board Builder Constructor
		 */
		public BoardBuilder()
		{
			m_BoardConfig = new HashMap<>();
			m_MoveMaker = Alliance.WHITE;
			m_EnPassantPawn = null;
		}

		public Map<Integer, Piece> GetBoardConfig()
		{
			return m_BoardConfig;
		}

		public void SetPiece(Piece piece)
		{
			m_BoardConfig.put(piece.GetCurrentCoordinate(), piece);
		}

		public void SetMoveMaker(Alliance alliance)
		{
			m_MoveMaker = alliance;
		}

		public void SetEnPassant(Pawn pawn)
		{
			m_EnPassantPawn = pawn;
		}

		/*********************************
		 * Builder's Build Function
		 * Call board's constructor with this builder's information.
		 */
		public Board Build()
		{
			return new Board(this);
		}

	}
}

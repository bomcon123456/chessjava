package com.chess.game.Board;

import java.util.HashMap;
import java.util.Map;

/*************************************************************************
 * BoardUtilities Class represents a state of a Chess game. A State will be created every time player makes a new move.
 * BoardUtilities consists of:
 * - "NUM_TILES": global variable represents how many tiles a board has.
 * - "RANK/COLUMN": Get TileID of a tile on a specific rank/column
 * ' "ALPHABET_BOARD": Get the algebraic notation of the board

 */

public class BoardUtilities
{
	public static final int NUM_TILES = 64;

	public static final boolean[] EIGHTH_RANK = CreateRankID(0);
	public static final boolean[] SEVENTH_RANK = CreateRankID(8);
	public static final boolean[] SIXTH_RANK = CreateRankID(16);
	public static final boolean[] FIFTH_RANK = CreateRankID(24);
	public static final boolean[] FOURTH_RANK = CreateRankID(32);
	public static final boolean[] THIRD_RANK = CreateRankID(40);
	public static final boolean[] SECOND_RANK = CreateRankID(48);
	public static final boolean[] FIRST_RANK = CreateRankID(56);

	public static final boolean[] FIRST_COLUMN = CreateColumnID(0);
	public static final boolean[] SECOND_COLUMN = CreateColumnID(1);
	public static final boolean[] SEVENTH_COLUMN = CreateColumnID(6);
	public static final boolean[] EIGHTH_COLUMN = CreateColumnID(7);

	public static final String[] ALPHABET_BOARD = CreateAlphabetBoard();
	public static final Map<String, Integer> POSITION_TO_COORDINATE = CreateAlphabetBoardMap();

	private BoardUtilities()
	{
		throw new RuntimeException("Shouldn't be here.");
	}

	private static String[] CreateAlphabetBoard() {
		return new String[] {
				"a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
				"a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
				"a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
				"a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
				"a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
				"a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
				"a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
				"a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"
		};
	}

	private static Map<String, Integer> CreateAlphabetBoardMap() {
		final Map<String, Integer> positionToCoordinate = new HashMap<>();
		for (int i = 0; i < NUM_TILES; i++) {
			positionToCoordinate.put(ALPHABET_BOARD[i], i);
		}
		return positionToCoordinate;
	}

	private static boolean[] CreateColumnID(int firstTileIdOfCol)
	{
		final boolean[] result = new boolean[64];
		for (int i = 0; i < NUM_TILES; i += 8)
		{
			result[i + firstTileIdOfCol] = true;
		}
		return result;
	}

	private static boolean[] CreateRankID(int firstTileIdOfRank)
	{
		final boolean[] result = new boolean[64];
		for (int i = 0; i < 8; i++)
		{
			result[i + firstTileIdOfRank] = true;
		}
		return result;
	}

	public static boolean IsCoordinateInBound(final int coord)
	{
		return ((coord >= 0) && (coord <=63));
	}

	public static String GetAlphabetLocation(int pos)
	{
		return ALPHABET_BOARD[pos];
	}
}

package com.chess.game.Board;

/*************************************************************************
 * MovePreview represents the status of the board after make the move
 * - "m_ResultBoard": the state after this move made.
 * - "m_Move": the move that we're considering.
 * - "m_MoveStatus": is this move valid?
 */
public class MovePreview
{
	private final Board m_ResultBoard;
	private final Move m_Move;
	private final MoveStatus m_MoveStatus;

	public MovePreview(final Board board, final Move move, final MoveStatus moveStatus)
	{
		m_ResultBoard = board;
		m_Move = move;
		m_MoveStatus = moveStatus;
	}

	public MoveStatus GetMoveStatus()
	{
		return m_MoveStatus;
	}

	public Board GetResultBoard()
	{
		return m_ResultBoard;
	}
}

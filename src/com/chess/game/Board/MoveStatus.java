package com.chess.game.Board;

/*************************************************************************
 * MoveStatus enum
 */
public enum MoveStatus
{
	VALID_MOVE_COMPLETE,
	INVALID_MOVE,
	LEAVES_PLAYER_IN_CHECK;

}

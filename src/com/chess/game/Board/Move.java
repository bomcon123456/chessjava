package com.chess.game.Board;

import com.chess.game.Piece.Pawn;
import com.chess.game.Piece.Piece;
import com.chess.game.Piece.PieceType;
import com.chess.game.Piece.Rook;

import static com.chess.game.Board.Board.*;
import static java.lang.Math.abs;

/*************************************************************************
 * Move abstract class represent a move that the player will make
 * Move consists of:
 * - "m_Board": The board where this move is made from.
 * - "m_Piece": The piece that will be moved.
 * - "m_DestinationCoord": The destination of the Piece.
 * - "m_IsFirstMove": Has this piece moved before?
 * - "NULL_MOVE": the global instance of type NULL_MOVE for easy handling null move.
 */

public abstract class Move
{
	protected final Board m_Board;
	protected final Piece m_Piece;
	protected final int m_DestinationCoordinate;
	protected final boolean m_IsFirstMove;

	public static final Move NULL_MOVE = new NullMove();

	/**
	 * Move's Constructor
	 * Standard constructor for valid move
	 */
	private Move(final Board board, final Piece piece, final int dest)
	{
		m_Board = board;
		m_Piece = piece;
		m_DestinationCoordinate = dest;
		m_IsFirstMove = m_Piece.GetIsFirstMove();
	}

	/**
	 * Move's Null constructor
	 */
	private Move(final Board board, final int dest)
	{
		m_Board = board;
		m_Piece = null;
		m_DestinationCoordinate = dest;
		m_IsFirstMove = false;
	}

	////////////////////////// Override ////////////////////////
	@Override
	public boolean equals(final Object other)
	{
		if(this == other)
		{
			return true;
		}
		if(!(other instanceof Move))
		{
			return false;
		}
		final Move otherMove = (Move)other;
		return (this.GetDestinationCoordinate() == otherMove.GetDestinationCoordinate() &&
				(this.GetMovedPiece() == otherMove.GetMovedPiece()) &&
				(this.GetPieceCurrentCoordinate() == otherMove.GetPieceCurrentCoordinate())
		);
	}

	@Override
	public int hashCode()
	{
		int result = 1;
		result = result * 31 + m_DestinationCoordinate;
		result = result * 31 + m_Piece.hashCode();
		//TODO do I need add piece's pos here?
		return result;
	}

	@Override
	public String toString()
	{
		return m_Piece.GetPieceType().toString() + BoardUtilities.GetAlphabetLocation(m_DestinationCoordinate);
	}
	////////////////////////// Override ////////////////////////

	////////////////////////// Public Func ////////////////////////

	public Board GetBoard()
	{
		return m_Board;
	}

	public final int GetPieceCurrentCoordinate()
	{
		return m_Piece.GetCurrentCoordinate();
	}

	public final int GetDestinationCoordinate()
	{
		return m_DestinationCoordinate;
	}

	public final Piece GetMovedPiece()
	{
		return m_Piece;
	}

	public boolean IsAttackMove()
	{
		return false;
	}

	public Piece GetAttackedPiece()
	{
		return null;
	}

	public boolean IsCastlingMove()
	{
		return false;
	}


	/**
	 * Create Board From Move
	 * First make a builder with the previous board's information, except for the piece that will be moved.
	 * Then add the new moved piece.
	 * Finally create the board via builder.
	 */
	public Board CreateBoardFromMove()
	{
		final BoardBuilder boardBuilder = new BoardBuilder();

		for(final Piece currentPiece : m_Board.GetCurrentPlayer().GetActivePieces())
		{
			if(!(m_Piece.equals(currentPiece)))
			{
				boardBuilder.SetPiece(currentPiece);
			}
		}

		for(final Piece currentPiece : m_Board.GetCurrentPlayer().GetOpponent().GetActivePieces())
		{
			boardBuilder.SetPiece(currentPiece);
		}

		boardBuilder.SetPiece(m_Piece.MovePiece(this));
		boardBuilder.SetMoveMaker(m_Board.GetCurrentPlayer().GetOpponent().GetAlliance());

		return boardBuilder.Build();
	}

	/**
	 * Create Board Before This Move (for undo)
	 * Make the previous board.
	 */
	public Board CreateBoardBeforeThisMove()
	{
		final BoardBuilder boardBuilder = new BoardBuilder();

		for(final Piece currentPiece : m_Board.GetAllActivePieces())
		{
			boardBuilder.SetPiece(currentPiece);
		}
		if(m_Board.GetEnPassantPawn()!=null)
		{
			boardBuilder.SetEnPassant(m_Board.GetEnPassantPawn());
		}
		boardBuilder.SetMoveMaker(m_Board.GetCurrentPlayer().GetAlliance());

		return boardBuilder.Build();
	}

	////////////////////////// Public Func ////////////////////////

	////////////////////////// Sub-Classes ////////////////////////
	/*************************************************************************
	 * Normal Move is the standard move (without attack and the piece is not Pawn)
	 */
	public static final class NormalMove extends Move
	{
		public NormalMove(final Board board, final Piece piece, final int dest)
		{
			super(board, piece, dest);
		}

		@Override
		public boolean equals(Object other)
		{
			return (((this == other) || (other instanceof NormalMove)) && (super.equals(other)));
		}
	}

	/*************************************************************************
	 * Attack Move is the base class for attack move
	 * Attack Move will have "m_AttackedPiece" represents the instance of the piece will be attacked.
	 */
	public static class AttackMove extends Move
	{
		final Piece m_AttackedPiece;

		public AttackMove(final Board board, final Piece piece, final Piece attacked, final int dest)
		{
			super(board, piece, dest);
			m_AttackedPiece = attacked;
		}

		@Override
		public boolean IsAttackMove()
		{
			return true;
		}

		@Override
		public Piece GetAttackedPiece()
		{
			return m_AttackedPiece;
		}

		@Override
		public int hashCode()
		{
			return m_AttackedPiece.hashCode() + super.hashCode();
		}

		@Override
		public boolean equals(final Object other)
		{
			if(this == other)
			{
				return true;
			}
			if(!(other instanceof AttackMove))
			{
				return false;
			}
			final AttackMove otherMove = (AttackMove) other;
			return ((super.equals(otherMove)) && (this.GetAttackedPiece().equals(otherMove.GetAttackedPiece())));
		}

        @Override
        public String toString()
        {
            return m_Piece.GetPieceType().toString() + "(x)" + BoardUtilities.GetAlphabetLocation(m_DestinationCoordinate);
        }
	}

	/*************************************************************************
	 * Normal Attack Move is the standard class for attack move (the piece is not a pawn)
	 * Attack Move will have "m_AttackedPiece" represents the instance of the piece will be attacked.
	 */
	public static final class NormalAttackMove extends AttackMove
	{
		public NormalAttackMove(final Board board, final Piece piece, final Piece attacked, final int dest)
		{
			super(board, piece, attacked, dest);
		}

		@Override
		public boolean equals(final Object other)
		{
			return this == other || other instanceof NormalAttackMove && super.equals(other);
		}

	}

			/////////////////// PAWN MOVE ////////////////////////
	/*************************************************************************
	 * Pawn Move is the standard class for pawn move
	 * Pawn Move is made so that we can separate completely the pawn's moving from other pieces for easy management.
	 */
	public static final class PawnMove extends Move
	{
		public PawnMove(final Board board, final Piece piece, final int dest)
		{
			super(board, piece, dest);
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnMove && super.equals(other);
		}

	}

	/*************************************************************************
	 * Pawn Attack Move is the standard class for pawn attack move
	 * Same reason as Pawn Move
	 */
	public static class PawnAttackMove extends AttackMove
	{

		public PawnAttackMove(final Board board, final Piece piece, final Piece attacked, final int dest)
		{
			super(board, piece, attacked, dest);
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnAttackMove && super.equals(other);
		}

	}

	/*************************************************************************
	 * Pawn En Passant Attack Move is the standard class for En Passant move situation
	 * En Passant is when the oppenent's pawn make a jump and its destination is on this pawn left's or right
	 * Then this pawn can jump diagonally 1 square ahead and still can attack the jumped piece.
	 */
	public static class PawnEnPassantAttackMove extends AttackMove
	{

		public PawnEnPassantAttackMove(final Board board, final Piece piece, final Piece attacked, final int dest)
		{
			super(board, piece, attacked, dest);
		}

		@Override
		public boolean equals(final Object other)
		{
			return this == other || other instanceof PawnEnPassantAttackMove  && super.equals(other);
		}


		/**
		 * Create Board From Move (Override)
		 * Same as the parent's function except skipping the attacked piece.
		 */
		@Override
		public Board CreateBoardFromMove()
		{
			final BoardBuilder boardBuilder = new BoardBuilder();
			for(final Piece curPiece : m_Board.GetCurrentPlayer().GetActivePieces())
			{
				if(!(m_Piece.equals(curPiece)))
				{
					boardBuilder.SetPiece(curPiece);
				}
			}
			for(final Piece curPiece : m_Board.GetCurrentPlayer().GetOpponent().GetActivePieces())
			{
				if(!(curPiece.equals(GetAttackedPiece())))
				{
					boardBuilder.SetPiece(curPiece);
				}
			}
			boardBuilder.SetPiece(m_Piece.MovePiece(this));
			boardBuilder.SetMoveMaker(m_Board.GetCurrentPlayer().GetOpponent().GetAlliance());
			return boardBuilder.Build();
		}
	}

	/*************************************************************************
	 * Pawn Jump is the standard class for pawn's 2-square move.
	 * When the pawn is never moved before and it's on the second/seventh rank, he can jump ahead 2 square.
	 */
	public static final class PawnJump extends Move
	{
		public PawnJump(final Board board, final Piece piece, final int dest)
		{
			super(board, piece, dest);
		}

		/**
		 * Create Board From Move (Override)
		 * Same as the parent's function
		 * Except tell the builder to make this pawn is the en passant pawn for the next board.
		 */
		@Override
		public Board CreateBoardFromMove()
		{
			final BoardBuilder boardBuilder = new BoardBuilder();
			for(final Piece piece : m_Board.GetCurrentPlayer().GetActivePieces())
			{
				if(!(m_Piece.equals(piece)))
				{
					boardBuilder.SetPiece(piece);
				}
			}
			for(final Piece piece : m_Board.GetCurrentPlayer().GetOpponent().GetActivePieces())
			{
				if(!(m_Piece.equals(piece)))
				{
					boardBuilder.SetPiece(piece);
				}
			}
			final Pawn movedPawn = (Pawn)m_Piece.MovePiece(this);
			boardBuilder.SetPiece(movedPawn);
			boardBuilder.SetEnPassant(movedPawn);
			boardBuilder.SetMoveMaker(m_Board.GetCurrentPlayer().GetOpponent().GetAlliance());
			return boardBuilder.Build();
		}

		@Override
		public boolean equals(final Object other) {
			return this == other || other instanceof PawnJump && super.equals(other);
		}

	}

	/*************************************************************************
	 * Pawn Promotion is the standard class for pawn's promotion.
	 * When the pawn is moved to the first rank of the opponent's board, the pawn can be promoted to Knight,Queen,Rook,...
	 * But we only implement promote to the queen because via Wiki 96% people will choose queen.
	 * "m_Move": The actual move that the pawn will move.
	 * "m_PromotedPawn": This pawn instance. (don't need this actually but still make it as a var for easy reading)
	 */
	public static final class PawnPromotionMove extends Move
	{
		final Move m_Move;
		final Pawn m_PromotedPawn;

		public PawnPromotionMove(final Move move) {
			super(move.GetBoard(), move.GetMovedPiece(), move.GetDestinationCoordinate());
			m_Move = move;
			m_PromotedPawn = (Pawn) move.GetMovedPiece();
		}

		/**
		 * Create Board From Move (Override)
		 * Same as the parent's function
		 * Except tell the builder to make this pawn to a new queen.
		 */
		@Override
		public Board CreateBoardFromMove()
		{
			final Board pawnMovedBoard = m_Move.CreateBoardFromMove();
			final BoardBuilder boardBuilder = new BoardBuilder();
			for(final Piece piece : pawnMovedBoard.GetCurrentPlayer().GetActivePieces())
			{
				if(!(m_PromotedPawn.equals(piece)))
				{
					boardBuilder.SetPiece(piece);
				}
			}
			for(final Piece piece : pawnMovedBoard.GetCurrentPlayer().GetOpponent().GetActivePieces())
			{
				boardBuilder.SetPiece(piece);
			}
			boardBuilder.SetPiece(m_PromotedPawn.GetPromotionPiece().MovePiece(this));
			boardBuilder.SetMoveMaker(pawnMovedBoard.GetCurrentPlayer().GetAlliance());
			return boardBuilder.Build();
		}

		@Override
		public boolean IsAttackMove() {
			return m_Move.IsAttackMove();
		}

		@Override
		public Piece GetAttackedPiece() {
			return m_Move.GetAttackedPiece();
		}

		@Override
		public int hashCode() {
			return m_Move.hashCode() + ( 31 * m_PromotedPawn.hashCode());
		}

		@Override
		public boolean equals(Object other) {
			return this == other || other instanceof PawnPromotionMove && (super.equals(other));
		}

		@Override
		public String toString()
		{
			return "Q"+ BoardUtilities.GetAlphabetLocation(m_DestinationCoordinate);
		}
	}
	/////////////////// PAWN MOVE ////////////////////////

	/*************************************************************************
	 * Castle Move is the base class for the king's castle move
	 * Castle Move is when the king swap places with the rook with some specified conditions (read wiki for more info)
	 * We divide this to two situation (Left(Queen side) - Right(King side)) so a base class should be made!
	 * - "m_CastleRook": The rook that will be swapped place with.
	 * - "m_CastleRookStart/DesPos": The tileID of the rook after/before moved (Don't need but for easy reading)
	 */
	public static class CastleMove extends Move
	{
		protected Rook m_CastleRook;
		protected int m_CastleRookStartPos;
		protected int m_CastleRookDesPos;

		public CastleMove(final Board board, final Piece piece, final int dest, final Rook rook, final int rookStart, final int rookDes)
		{
			super(board, piece, dest);
			m_CastleRook = rook;
			m_CastleRookStartPos = rookStart;
			m_CastleRookDesPos = rookDes;
		}

		public Rook GetCastleRook()
		{
			return m_CastleRook;
		}

		@Override
		public boolean IsCastlingMove()
		{
			return true;
		}

		/**
		 * Create Board From Move (Override)
		 * Same as the parent's function
		 * Except tell the builder to change the pawn and king with new information.
		 */
		@Override
		public Board CreateBoardFromMove()
		{
			final BoardBuilder boardBuilder = new BoardBuilder();
			for(final Piece currentPiece : m_Board.GetCurrentPlayer().GetActivePieces())
			{
				if(currentPiece.GetAlliance() == m_Board.GetCurrentPlayer().GetAlliance())
				{
					if(currentPiece.GetPieceType() == PieceType.ROOK)
					{
						if(abs(currentPiece.GetCurrentCoordinate() - m_DestinationCoordinate) == 1 || abs(currentPiece.GetCurrentCoordinate() - m_DestinationCoordinate) == 2)
						{
							continue;
						}
					}
				}
				if(!(m_Piece.equals(currentPiece)))
				{
					boardBuilder.SetPiece(currentPiece);
				}
			}

			for(final Piece currentPiece : m_Board.GetCurrentPlayer().GetOpponent().GetActivePieces())
			{
				boardBuilder.SetPiece(currentPiece);
			}

			// m_Piece would be the King in this situation.
			boardBuilder.SetPiece(m_Piece.MovePiece(this));
			boardBuilder.SetPiece(new Rook(m_CastleRook.GetAlliance(), m_CastleRookDesPos));
			boardBuilder.SetMoveMaker(m_Board.GetCurrentPlayer().GetOpponent().GetAlliance());

			return boardBuilder.Build();
		}

		@Override
		public int hashCode()
		{
			int result = super.hashCode();
			result = result * 31 + m_CastleRook.hashCode();
			result = result * 31+ m_CastleRookDesPos;
			return result;
		}

		@Override
		public boolean equals(final Object other)
		{
			if (this == other)
			{
				return true;
			}
			if (!(other instanceof CastleMove))
			{
				return false;
			}
			final CastleMove otherCastleMove = (CastleMove) other;
			return super.equals(otherCastleMove) && m_CastleRook.equals(otherCastleMove.GetCastleRook());
		}

	}

	/*************************************************************************
	 * King SideCastle Move is the standard class for the king's queen-side castle move
	 * King will be on G1 (WHITE)
	 */
	public static final class KingSideCastleMove extends CastleMove
	{
		public KingSideCastleMove(final Board board, final Piece piece, final int dest, final Rook rook, final int rookStart, final int rookDes)
		{
			super(board, piece, dest, rook, rookStart, rookDes);
		}

		@Override
		public boolean equals(final Object other)
		{
			if (this == other)
			{
				return true;
			}
			if (!(other instanceof KingSideCastleMove))
			{
				return false;
			}
			final KingSideCastleMove otherKingSideCastleMove = (KingSideCastleMove) other;
			return super.equals(otherKingSideCastleMove) && m_CastleRook.equals(otherKingSideCastleMove.GetCastleRook());
		}


		//PGN Convention
		@Override
		public String toString()
		{
			return "O-O";
		}
	}

	/*************************************************************************
	 * Queen SideCastle Move is the standard class for the king's queen-side castle move
	 * King will be on C1 (WHITE)
	 */
	public static final class QueenSideCastleMove extends CastleMove
	{
		public QueenSideCastleMove(final Board board, final Piece piece, final int dest, final Rook rook, final int rookStart, final int rookDes)
		{
			super(board, piece, dest, rook, rookStart, rookDes);
		}

		@Override
		public boolean equals(final Object other)
		{
			if (this == other)
			{
				return true;
			}
			if (!(other instanceof QueenSideCastleMove))
			{
				return false;
			}
			final QueenSideCastleMove otherQueenSideCastleMove = (QueenSideCastleMove) other;
			return super.equals(otherQueenSideCastleMove) && m_CastleRook.equals(otherQueenSideCastleMove.GetCastleRook());
		}

		//PGN Convention
		@Override
		public String toString()
		{
			return "O-O-O";
		}
	}

	/*************************************************************************
	 * Null Move
	 * For moves that is not valid.
	 */
	public static final class NullMove extends Move
	{
		public NullMove()
		{
			super(null, -1);
		}

		@Override
		public Board CreateBoardFromMove()
		{
			throw new RuntimeException("Shouldn't CreateBoardFromMove null moves.");
		}
	}

	/*************************************************************************
	 * Factory Design Pattern
	 * This class is made so that clients only get moves via this, rather than go somewhere else to get those.
	 */
	public static final class MoveFactory
	{
		private MoveFactory()
		{
			throw new RuntimeException("Shouldn't be instantiable.");
		}

		public static Move CreateMove(final Board board, final int currentCoord, final int destinationCoord)
		{
			for(final Move curMove : board.GetAllLegalMoves())
			{
				if((curMove.GetPieceCurrentCoordinate() == currentCoord) && (curMove.GetDestinationCoordinate() == destinationCoord))
				{
					return curMove;
				}
			}
			return NULL_MOVE;
		}
	}
	////////////////////////// Sub-Classes ////////////////////////

}


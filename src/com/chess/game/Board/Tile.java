package com.chess.game.Board;

import com.chess.game.Piece.Piece;

import java.util.HashMap;
import java.util.Map;

/*************************************************************************
 * Tile class is the base class for the tile of chess game
 * Tile represents the actual tile of the board, consisting information like coordinate and the piece on it.
 * There are 64 tiles of the chess board (8 tiles/ rank).
 * "EMPTY_TILES_CACHE": This is the cache consisting 64 empty tiles (with coordinate from 0-63), so when the board needs
 * to add tiles to the board list, it can takes these immediately instead of making another one (reduces memory, no info loss)
 * "m_TileID": ID, also the coordinate of the tiles
 * Chessboard represent in tiles:
 * 00-01-02-03-04-05-06-07
 * ...............
 * 56-57-58-59-60-61-62-63
 */
public abstract class Tile
{
	private static final Map<Integer, EmptyTile> EMPTY_TILES_CACHE = InitiateEmptyTiles();
	protected final int m_TileID;

	Tile(final int tileID)
	{
		m_TileID = tileID;
	}

	////////////////////////// NON-ABSTRACT FUNCTION ////////////////////////////////
	public final int GetTileCoordinate()
	{
		return m_TileID;
	}

	/**
	 * Initiate Empty Tiles
	 * Create 64 empty tiles and save to a cache for later use.
	 */
	private static Map<Integer, EmptyTile> InitiateEmptyTiles()
	{
		final Map<Integer, EmptyTile> result = new HashMap<>();

		for (int i = 0; i < BoardUtilities.NUM_TILES; i++)
		{
			result.put(i, new EmptyTile(i));
		}
		return result;
	}

	/**
	 * Create Tile (will be use in Board class)
	 * Create the tile with predefine information.
	 * If there is a piece on this tile, create a OccupideTile and return it.
	 * If this is not, return the previously-created EmptyTile and return it.
	 */
	public static Tile CreateTile(final int tileID, final Piece piece)
	{
		return piece != null ? new OccupiedTile(tileID, piece) : EMPTY_TILES_CACHE.get(tileID);
	}

	////////////////////////// NON-ABSTRACT FUNCTION ////////////////////////////////


	public abstract boolean IsTileOccupied();

	public abstract Piece GetPiece();

	/**
	 *  These class should be static because:
	 * - Less memory spaces
	 * - No need for reference of object to the outer class.
	 * - Can't access non-static member of the outer class.
	 */

	/*************************************************************************
	 * Empty Tile Class represents a tile without a piece on it.
	 */
	public static final class EmptyTile extends Tile
	{
		EmptyTile(final int tileID)
		{
			super(tileID);
		}

		@Override
		public boolean IsTileOccupied()
		{
			return false;
		}

		@Override
		public Piece GetPiece()
		{
			return null;
		}
	}

	/*************************************************************************
	 * Empty Tile Class represents a tile with a piece on it.
	 * "m_PieceOnTile": That piece's instance.
	 */
	public static final class OccupiedTile extends Tile
	{
		private Piece m_PieceOnTile;

		OccupiedTile(final int tileID, final Piece piece)
		{
			super(tileID);
			m_PieceOnTile = piece;
		}

		@Override
		public boolean IsTileOccupied()
		{
			return true;
		}

		@Override
		public Piece GetPiece()
		{
			return m_PieceOnTile;
		}
	}

}
